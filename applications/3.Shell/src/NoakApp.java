import noak.manager.Shell;
import noak.utils.Delay;

public class NoakApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String ver = Shell.getVersion();
		System.out.println("Version(版本)=" + ver);

		while (true) {
			//usage @ noakcl : setvar args "string", for example: setvar args "1234567890"
			String str = Shell.getArgs();
			System.out.println("Args(参数)=" + str);
			Delay.msDelay(1000);
		}
	}

}
