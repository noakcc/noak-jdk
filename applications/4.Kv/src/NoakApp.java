import java.io.IOException;

import noak.manager.Kv;

public class NoakApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			Kv.set("K1", "this is a kv test, 0123456789.".getBytes());
			Kv.set("K2", "这是键值存储区测试，0123456789。".getBytes());
			byte[] val1 = Kv.get("K1");
			byte[] val2 = Kv.get("K2");
			System.out.println("K1=" + new String(val1));
			System.out.println("K2=" + new String(val2));
			Kv.delete("K1");
			Kv.delete("K2");
			Kv.format();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
