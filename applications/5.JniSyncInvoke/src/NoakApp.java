import jvm.driver.*;

class MyCalc {

	private Driver drv = null;
	private static MyCalc _me;

	private MyCalc() {
		// 用户JNI调用，本例程中驱动标识0表示‘计算器’类型。实际使用可用于区分不同类型驱动。
		drv = new Driver(0);
	}

	/**
	 * 获取实例对象
	 * 
	 * @return 实例对象
	 */
	public static MyCalc getInstance() {
		if (_me == null) {
			_me = new MyCalc();
		}
		return _me;
	}

	/**
	 * 计算两个数的代数和
	 * 
	 * @param a 数a
	 * @param b 数b
	 * @return a+b
	 */
	public int add(int a, int b) {
		// 本例程中控制码0表示‘两数代数和’计算。
		int c = drv.invokeSync(0, a, b);
		return c;
	}

	/**
	 * 计算两个数的代数c差
	 * 
	 * @param a 数a
	 * @param b 数b
	 * @return a-b
	 */
	public int sub(int a, int b) {
		// 本例程中控制码1表示‘两数代数差’计算。
		int c = drv.invokeSync(1, a, b);
		return c;
	}
}

/**
 * 该例程演示JNI同步调用。
 * 例程实现了'计算器'驱动类，通过该例程用户可以很好的了解框架的驱动同步调用模型。
 * 例程测试需要配合
 * https://gitee.com/noakcc/noak/tree/master/firmware/noak-win32/noak-win32.exe。
 */
public class NoakApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MyCalc calc = MyCalc.getInstance();

		System.out.println("a+b=" + calc.add(100, 200));
		System.out.println("a-b=" + calc.sub(200, 100));

		while (true) {
		}

	}

}
