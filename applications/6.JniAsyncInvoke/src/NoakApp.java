import java.io.IOException;

import jvm.driver.*;
import noak.utils.Delay;

class MyMusic extends Thread {

	private Driver drv = null;
	private static MyMusic _me;

	private MyMusic() {
		// 用户JNI调用，本例程中驱动标识2表示‘音乐’类型。实际使用可用于区分不同类型驱动。
		drv = new Driver(2);
	}

	/**
	 * 获取实例对象
	 * 
	 * @return 实例对象
	 */
	public static MyMusic getInstance() {
		if (_me == null) {
			_me = new MyMusic();
		}
		return _me;
	}

	@Override
	public void run() {

		System.out.println("循环播放音乐线程启动...");

		while (true) {
			try {
				// 1. 此处异步调用音乐播放, 如果使用同步调用, 则其他线程都会被阻塞住, 本例程中主线程会被阻塞住!!!
				// 2. 因为音乐播放占用的JNI时间很久, 采用异步调用时框架执行效率最高。
				drv.invokeAsync(this, 0);
				drv.invokeAsync(this, 1);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}

/**
 * 该例程演示JNI异步调用。
 * 例程实现了'音乐'驱动类，通过该例程用户可以很好的了解框架的驱动异步调用模型。
 * 例程测试需要配合
 * https://gitee.com/noakcc/noak/tree/master/firmware/noak-win32/noak-win32.exe。
 */
public class NoakApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MyMusic music = MyMusic.getInstance();
		music.start();

		while (true) {
			System.out.println("主线程还在运行.");
			Delay.msDelay(1000);
		}

	}

}
