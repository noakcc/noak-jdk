import java.io.IOException;

import jvm.driver.*;

abstract interface IMyKeyboardEvent {
	/**
	 * 键盘敲击事件接口
	 * 
	 * @param keyCode 键盘码
	 * @param int    事件计数器
	 */
	public abstract void onClick(int keyCode, int counter);
}

class MyKeyboard {

	private Driver drv = null;
	private IMyKeyboardEvent callback;
	private static MyKeyboard _me;

	private MyKeyboard() {
		// 用户JNI调用，本例程中驱动标识1表示‘‘键盘’类型。实际使用可用于区分不同类型驱动。
		drv = new Driver(1);
	}

	/**
	 * 获取实例对象
	 * 
	 * @return 实例对象
	 */
	public static MyKeyboard getInstance() {
		if (_me == null) {
			_me = new MyKeyboard();
		}
		return _me;
	}

	/**
	 * 注册键盘事件回调
	 * 
	 * @param lc 回调
	 */
	public void register(IMyKeyboardEvent lc) {
		callback = lc;
		try {
			// 注册事件监听
			drv.registerEventListener(new IDriverEventListener() {

				@Override
				public void onEvent(int id, int message, int wParam, int lParam, int counter, long time) {
					// 调用事件回调，message传递keyCode
					callback.onClick(message, counter);
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void unRegister() {
		// 注销事件监听
		drv.unregisterEventListener();
	}

	public void waitEvent() {
		try {
			// 等待事件到达
			drv.waitEvent(Driver.WAIT_FOREVER);
		} catch (InterruptedException e) {
		}
	}
}

/**
 * 该例程演示JNI事件获取
 * 例程实现了'键盘'驱动类，通过该例程用户可以很好的了解框架的驱动事件模型。
 * 例程测试需要配合
 * https://gitee.com/noakcc/noak/tree/master/firmware/noak-win32/noak-win32.exe。
 */
public class NoakApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MyKeyboard keyboard = MyKeyboard.getInstance();

		keyboard.register(new IMyKeyboardEvent() {
			@Override
			public void onClick(int keyCode, int counter) {
				System.out.println("onClick : keyCode=" + keyCode + "  counter=" + counter);

				if (keyCode == 27) {
					/* 如果按键为'Esc'，则应用强制退出 */
					keyboard.unRegister();
					System.exit(0);
				}
			}
		});

		while (true) {
			keyboard.waitEvent();
		}

	}

}
