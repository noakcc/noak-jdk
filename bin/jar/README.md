环境要求：JavaSE-1.8或更高

用法1：java -jar noak-jit.jar -v -bp jdk.jar -cp app.jar META-INF -o app

​			 适用于jdk和app独立存在情况。

用法2：java -jar noak-jit.jar -v -bp "" -cp app.jar META-INF -o app

​			 适用于jdk与app合并打包情况。

