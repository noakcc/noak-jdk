// nd.cpp
//

#include <windows.h>
#include <stdint.h>
#include <stdio.h>
#include <conio.h>
#include <direct.h>
#include <string.h>
#include <sys/stat.h>
#include <process.h>

#define NDL_WAIT1_TIMEOUT   1 * 1000   //秒
#define NDL_WAIT10_TIMEOUT  10 * 1000  //秒

#define NDL_RETRY_TIMES     5          //次

#define NDL_ERR_NO      0
#define NDL_ERR_ME      1
#define NDL_ERR_UE      2

#define PACKET_HEADER           	3     /* start, block, block-complement */  
#define PACKET_TRAILER          	2     /* CRC bytes */  
#define PACKET_OVERHEAD         	(PACKET_HEADER + PACKET_TRAILER) 
#define PACKET_SIZE                 (128 + PACKET_OVERHEAD)
#define PACKET_1KSIZE               (1024 + PACKET_OVERHEAD)

/* ASCII control codes: */
#define CC_SOH 	    0x01      	/* start of 128-byte data packet */  
#define CC_STX 	    0x02      	/* start of 1024-byte data packet */  
#define CC_ETX 	    0x03      	/* end of data packet !!!Extended */  
#define CC_EOT 	    0x04      	/* end of transmission */  
#define CC_ACK 	    0x06      	/* receive OK */  
#define CC_NAK 	    0x15      	/* receiver error; retry */  
#define CC_CAN 	    0x18      	/* two of these in succession aborts transfer */  
#define CC_C 	    0x43      	/* character 'C' */

#define CTL_C  0x03
#define CTL_X  0x24


typedef int (*fcomReadChar)(void);
typedef void (*fcomWriteChar)(int ci);
typedef void (*fcomClear)(void);

static fcomReadChar comReadChar = NULL;
static fcomWriteChar comWriteChar = NULL;
static fcomClear comClear = NULL;

static int taskExit = 0;

/* 读取字节数据
 */
static int readByte(void)
{
    return comReadChar();
}

/* 写入指定数量数据
 */
static void writeBuffer(uint8_t* buffer, uint16_t size)
{
    for (int i = 0; i < size; i++)
    {
        comWriteChar(buffer[i]);
    }
}

/* 写入字节数据
 */
static void writeByte(uint8_t b)
{
    comWriteChar(b);
}

static FILE* loadFile(char* file, uint32_t* size, time_t* timecreate)
{
    time_t time = 0;

    FILE* fp = fopen(file, "rb+");

    if (fp == NULL)
    {
        printf("file '%s' not exist.\n", file);
        system("pause");
        exit(-1);
    }

    fseek(fp, 0, SEEK_END);//定位文件指针到文件尾。

    *size = ftell(fp);//获取文件指针偏移量，即文件大小。

    fseek(fp, 0, SEEK_SET);

    struct stat result = { 0 };

    if (stat(file, &result) == 0)
    {
        time = result.st_mtime;
    }

    *timecreate = time;

    return fp;
}

static void unloadFile(FILE* fp)
{
    if (fp)
    {
        fclose(fp);
    }
}

static int wait(const char* tag, uint32_t delayms, uint32_t times)
{
    int rsp = 0;

    while (times--)
    {
        if ((rsp = readByte()) < 0)
        {
            Sleep(delayms);
            continue;
        }
        
        return rsp;
    }

    printf("wait '%s' timeout.\n", tag);

    return -1;
}

/* 多项式为X^16 + X^12 + X^5 +1 */
uint16_t ymodemCRC16(uint8_t* data, uint32_t sz)
{
    uint16_t crc = 0;
    uint8_t i;

    while (sz--)
    {
        crc = crc ^ (data[0] << 8);

        for (i = 0; i < 8; i++)
        {
            if (crc & 0x8000)
            {
                crc = (crc << 1) ^ 0x1021;
            }
            else
            {
                crc = crc << 1;
            }
        }

        data++;
    }

    return crc;
}

static uint8_t writeStart(char* uname, time_t mtime, uint32_t size)
{
    uint8_t packetBuffer[PACKET_SIZE] = { 0 };
    uint8_t* p;
    int8_t times = NDL_RETRY_TIMES;

    printf("write start : uname=%s  -  mtime=%lld  -  size=%d.\n", uname, mtime, size);

    p = packetBuffer;

    *p++= CC_SOH;
    *p++ = 0x00;
    *p++ = ~0x00;

    int szN = strlen(uname);
    
    memcpy(p, uname, szN); p += szN;

    *p++ = '\0';

    _itoa(size, (char*)p, 10);

    int szS = strlen((char*)p);

    p += szS;

    *p++ = ' ';

    _ltoa((long)mtime, (char*)p, 8);

    int szM = strlen((char*)p);

    p += szM;

    *p++ = '\0';

    uint16_t crc = ymodemCRC16(packetBuffer + PACKET_HEADER, PACKET_SIZE - PACKET_OVERHEAD);

    packetBuffer[PACKET_SIZE - 2] = (crc >> 8) & 0xff;
    packetBuffer[PACKET_SIZE - 1] = crc & 0xff;

start:
    writeBuffer(packetBuffer, PACKET_SIZE);

    int ack = 0;

    while ((ack = wait("ACK", 10, NDL_WAIT10_TIMEOUT / 10)) == CC_C);
    
    if (ack == CC_ACK)
    {
        while ((ack = wait("C", 10, NDL_WAIT10_TIMEOUT / 10)) == CC_ACK);

        if (ack == CC_C)
        {
            return NDL_ERR_NO;
        }
    }

    printf("ACK@S error(%x).\n", ack);

    if (ack == CC_CAN)
    {
        return NDL_ERR_UE;
    }

    if (ack == CC_NAK)
    {
        if (times-- > 0)
        {
            Sleep(100);
            goto start;
        }
    }

    return NDL_ERR_ME;
}

static uint8_t writeUpdate(uint8_t* buffer, uint32_t size, uint8_t pn)
{
    int8_t times = NDL_RETRY_TIMES;

    printf("write update : size=%d  -  pn=%d.\n", size, pn);

    buffer[0] = (size > 128) ? CC_STX : CC_SOH;
    buffer[1] = pn;
    buffer[2] = ~pn;

    uint32_t psize = (size > 128) ? PACKET_1KSIZE : PACKET_SIZE;

    uint16_t crc = ymodemCRC16(buffer + PACKET_HEADER, psize - PACKET_OVERHEAD);

    buffer[psize - 2] = (crc >> 8) & 0xff;
    buffer[psize - 1] = crc & 0xff;

update:
    writeBuffer(buffer, psize);

    int ack = 0;

    while ((ack = wait("ACK", 10, NDL_WAIT10_TIMEOUT / 10)) == CC_C);

    if (ack == CC_ACK)
    {
        return NDL_ERR_NO;
    }

    printf("ACK@U error(%x).\n", ack);

    if (ack == CC_CAN)
    {
        return NDL_ERR_UE;
    }

    if (ack == CC_NAK)
    {
        if (times-- > 0)
        {
            Sleep(100);
            goto update;
        }
    }

    return NDL_ERR_ME;
}

static uint8_t writeStop(void)
{
    int8_t times = NDL_RETRY_TIMES;
    int cc;

    printf("write stop.\n");

    cc = 2;
    while (cc--)
    {
        writeByte(CC_EOT);
    }

    int ack;

    do
    {
        ack = wait("ACK", 10, NDL_WAIT10_TIMEOUT / 10);
    } while (ack == CC_C || ack == CC_NAK);

    if (ack == CC_ACK)
    {
        return NDL_ERR_NO;
    }

    printf("ACK@E error(%x).\n", ack);

    if (ack == CC_CAN)
    {
        return NDL_ERR_UE;
    }

    return NDL_ERR_ME;
}

static uint8_t writeFinish(uint8_t error)
{
    uint8_t packetBuffer[PACKET_SIZE] = { 0 };
    int8_t times = NDL_RETRY_TIMES;

    printf("write finish : code=%d.\n", error);

finish:
    if (error == NDL_ERR_UE)
    {
        return error;
    }

    if (error == NDL_ERR_ME)
    {
        int cc = 5;

        while (cc--)
        {
            writeByte(CC_CAN);
        }

        return error;
    }

    error = writeStop();

    if (error != NDL_ERR_NO)
    {
        goto finish;
    }

    error = writeUpdate(packetBuffer, 128, 0);

    if (error != NDL_ERR_NO)
    {
        goto finish;
    }

    return error;
}

static void appdl(void)
{
    char* cmd = (char*)"app dl\n";

    for (uint8_t i = 0; i < strlen(cmd); i++)
    {
        comWriteChar(cmd[i]);
    }
}

static void appDownload(void)
{
    comClear();
    appdl();
    Sleep(100);
    comClear();
}

static uint8_t download(char* file, char* uname)
{
    uint8_t packetBuffer[PACKET_1KSIZE] = { 0 };
    uint32_t size = 0;
    time_t mtime = 0;
    uint8_t error = 0;

    printf("file : '%s'  -   uname : '%s'\n", file, uname);

    FILE* fp = loadFile(file, &size, &mtime);

    appDownload(); /* 启动'应用下载' */

    int times = NDL_RETRY_TIMES;
    int ack;
    do
    {
        ack = wait("C", 10, NDL_WAIT1_TIMEOUT / 10);
        if (ack < 0)
        {
            times--;
        }
    } while (ack != CC_C && times);

    if (ack != CC_C)
    {
        error = NDL_ERR_ME;
    }

    if (error == NDL_ERR_NO)
    {
        error = writeStart(uname, mtime, size);
    }

    uint32_t seek = 0;
    uint32_t wsize = 0;
    uint8_t pn = 1;
    uint32_t _size = size;

    while (error == NDL_ERR_NO && _size)
    {
        fseek(fp, seek, SEEK_SET);

        wsize = (_size > 1024) ? 1024 : _size;

        if (fread(packetBuffer + PACKET_HEADER, 1, wsize, fp) != wsize)
        {
            error = NDL_ERR_ME;
            printf("file read error.\n");
            system("pause");
            break;
        }

        error = writeUpdate(packetBuffer, wsize, pn);

        if (error != NDL_ERR_NO)
        {
            break;
        }

        printf("download : %d.\n", (size - _size) * 100 / size);

        seek += wsize;
        _size -= wsize;
        pn++;
    }

    error = writeFinish(error);

    if (error == NDL_ERR_NO)
    {
        printf("download : 100.\n");
    }

    unloadFile(fp);

    return error;
}

static char* openFile(void)
{
    char* file = (char*)malloc(MAX_PATH);
    char* path = (char*)malloc(MAX_PATH);

    OPENFILENAME ofn = { 0 };
    ofn.lStructSize = sizeof(OPENFILENAME);
    ofn.hwndOwner = NULL;
    ofn.lpstrFilter = "noak app(*.j2m)\0*.j2m;\0";
    ofn.nFilterIndex = 1;
    ofn.lpstrFile = file;
    ofn.lpstrFile[0] = '\0';
    ofn.nMaxFile = MAX_PATH;
    ofn.lpstrInitialDir = _getcwd(path, MAX_PATH);
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

    BOOL nRet = GetOpenFileName(&ofn);

    free(path);

    if (!nRet)
    {
        free(file);
        file = NULL;
    }

    return file;
}

static void taskX(PVOID param)
{
#define _TEST   0

    while (!taskExit)
    {
    #if !_TEST
        char* file = openFile();
    #else
        char* file = "D:\\test.j2m";
    #endif
        if (file != NULL)
        {
            char uname[MAX_PATH] = { 0 };

            printf("load : %s \n", file);
            printf("input app unique name :");

            if (scanf("%s", uname) == 1)
            {
                uint8_t dlErr = download(file, uname);

                if (dlErr == NDL_ERR_NO)
                {
                    printf("download %s successful.\n", uname);
                }
                else
                {
                    printf("download %s fail(%d).\n", uname, dlErr);
                }
            }
        #if !_TEST
            free(file);
        #endif
        }

        taskExit = 1;
    }
}

void ndCreateTask(int (*fcomReadChar)(void), void (*fcomWriteChar)(int ci), void (*fcomClear)(void))
{
    taskExit = 0;

    comReadChar = fcomReadChar;
    comWriteChar = fcomWriteChar;
    comClear = fcomClear;

    _beginthread(taskX, 0, NULL);
}

void ndWaitTask(void)
{
    while (!taskExit)
    {
        Sleep(100);
    }
}
