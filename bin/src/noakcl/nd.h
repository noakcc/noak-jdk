#pragma once


void ndCreateTask(int (*fcomReadChar)(void), void (*fcomWriteChar)(int ci), void (*fcomClear)(void));
void ndWaitTask(void);
