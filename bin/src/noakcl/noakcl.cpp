// noakcl.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <windows.h>
#include <stdint.h>
#include <stdio.h>
#include "nd.h"
#include "nx.h"

#define NCL_COM_BAUDRATE    115200


static HANDLE hCom = NULL;

static void setComTimeout(void)
{
    COMMTIMEOUTS timeOuts = { 0 };

    /* 超时1毫秒读返回 */
    timeOuts.ReadIntervalTimeout = 0;
    timeOuts.ReadTotalTimeoutMultiplier = 0;
    timeOuts.ReadTotalTimeoutConstant = 1;
    /* 直到写完成后返回 */
    timeOuts.WriteTotalTimeoutConstant = 0;
    timeOuts.WriteTotalTimeoutMultiplier = 0;

    SetCommTimeouts(hCom, &timeOuts); //设置超时 
}

extern void enumSerial(void);

/* 终端串口打开
 */
static void comOpen(char* COM, int baudRate)
{
    char comPath[32] = { "\\\\.\\" };

    strcat(comPath, COM);

    hCom = CreateFile(comPath, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
    if (hCom == (HANDLE)-1)
    {
        printf("%s: open fail(%d).\n", COM, GetLastError());
        enumSerial();
        system("pause");
        exit(-1);
    }
    else
    {
        DCB wdcb;
        GetCommState(hCom, &wdcb);
        wdcb.BaudRate = baudRate;//波特率：baudRate
        wdcb.fBinary = TRUE;
        wdcb.ByteSize = DATABITS_8;
        wdcb.StopBits = ONESTOPBIT;
        wdcb.Parity = NOPARITY;
        wdcb.fRtsControl = RTS_CONTROL_DISABLE;
        wdcb.fDtrControl = DTR_CONTROL_DISABLE;
        SetCommState(hCom, &wdcb);
    }

    setComTimeout();

    PurgeComm(hCom, PURGE_TXCLEAR);
    PurgeComm(hCom, PURGE_RXCLEAR);
}

/* 串口清理
 */
static void comClear(void)
{
    PurgeComm(hCom, PURGE_TXCLEAR);
    PurgeComm(hCom, PURGE_RXCLEAR);
}

/* 串口读取字符
 */
static int comReadChar(void)
{
    DWORD rCount = 0;
    uint8_t cc[1] = { 0 };

    if (!ReadFile(hCom, (LPVOID)cc, 1, &rCount, NULL) || rCount == 0)
    {
        return -1;
    }

    return cc[0];
}

/* 串口写入字符
 */
static void comWriteChar(int ci)
{
    DWORD wCount = 0;
    char cc[1] = { (char)ci };

    WriteFile(hCom, (LPCVOID)cc, 1, &wCount, NULL);
}

static void consoleInit(void)
{
    SetConsoleTitle("ncl");

    CONSOLE_FONT_INFOEX cfi = { 0 };

    cfi.cbSize = sizeof(cfi);
    cfi.dwFontSize.X = 8;
    cfi.dwFontSize.Y = 18;
    cfi.FontWeight = FW_NORMAL;
    wcscpy(cfi.FaceName, L"\x9ed1\x4f53");  /* 黑体16号 */
    SetCurrentConsoleFontEx(GetStdHandle(STD_OUTPUT_HANDLE), FALSE, &cfi);

    system("chcp 65001"); /* 活动页:UTF-8编码 */
}

int main(int argc, char* argv[])
{
#define _TEST   0

    consoleInit();

#if !_TEST
    if (argc < 2)
    {
        printf("usage: noakcl COM?   : start the command line terminal through the COM port.\n");
        system("pause");
        exit(-1);
    }
    char* COM = argv[1];
#else
    char* COM = (char*)"COM15";
#endif

    comOpen(COM, NCL_COM_BAUDRATE);

    printf("%s: open success, ncl has been launched.\n\n", COM);
    printf("'TAB'    : show command list.\n");
    printf("'CTRL+D' : start the app download.\n");
    printf("'CTRL+C' : ncl exit.\n");

    char newTitle[MAX_PATH] = { 0 };

    sprintf_s(newTitle, sizeof(newTitle), "ncl @%s : baud rate=%d, 8, N, 1", COM, NCL_COM_BAUDRATE);

    SetConsoleTitle(newTitle);

    while (1)
    {
        system("pause");
        system("cls");

        nxCreateTask(comReadChar, comWriteChar, comClear);
        nxWaitTask();

        ndCreateTask(comReadChar, comWriteChar, comClear);
        ndWaitTask();
    }
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
