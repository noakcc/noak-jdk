// nx.cpp
//

#include <windows.h>
#include <stdint.h>
#include <stdio.h>
#include <conio.h>
#include <process.h>

#define CTL_C  0x03
#define CTL_D  0x04

typedef int (*fcomReadChar)(void);
typedef void (*fcomWriteChar)(int ci);
typedef void (*fcomClear)(void);

static fcomReadChar comReadChar = NULL;
static fcomWriteChar comWriteChar = NULL;
static fcomClear comClear = NULL;

static int taskExit = 0;
static int taskXExit = 0;
static int taskCExit = 0;

static void command(void)
{
    char* hello = (char*)"noak\n";

    comClear();

    for (uint8_t i = 0; i < strlen(hello); i++)
    {
        comWriteChar(hello[i]);
    }
}

static void taskX2COM(PVOID param)
{
    while (!taskExit)
    {
        int ci = _getch();

        if (ci == CTL_C)
        {
            printf("^Exit.\n");
            exit(0);
        }
        if (ci == CTL_D)
        {
            printf("^Download.\n");
            taskExit = 1;
            continue;
        }

        comWriteChar((ci == 0) ? 0xE0 : ci);

        Sleep(0);
    }

    taskXExit = 1;
}

static void taskCOM2X(PVOID param)
{
    while (!taskExit)
    {
        int ci = comReadChar();
        if (ci >= 0)
        {
            putchar(ci);
        }

        Sleep(0);
    }

    taskCExit = 1;
}

void nxCreateTask(int (*fcomReadChar)(void), void (*fcomWriteChar)(int ci), void (*fcomClear)(void))
{
    taskCExit = 0;
    taskXExit = 0;
    taskExit = 0;

    comReadChar = fcomReadChar;
    comWriteChar = fcomWriteChar;
    comClear = fcomClear;

    _beginthread(taskX2COM, 0, NULL);
    _beginthread(taskCOM2X, 0, NULL);

    command(); /* 发送'用户'指令 */
}

void nxWaitTask(void)
{
    while (!taskCExit || !taskXExit)
    {
        Sleep(100);
    }
}
