#pragma once


void nxCreateTask(int (*fcomReadChar)(void), void (*fcomWriteChar)(int ci), void (*fcomClear)(void));
void nxWaitTask(void);
