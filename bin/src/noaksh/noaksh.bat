::[Bat To Exe Converter]
::
::YAwzoRdxOk+EWAnk
::fBw5plQjdG8=
::YAwzuBVtJxjWCl3EqQJgSA==
::ZR4luwNxJguZRRnk
::Yhs/ulQjdF+5
::cxAkpRVqdFKZSDk=
::cBs/ulQjdF+5
::ZR41oxFsdFKZSDk=
::eBoioBt6dFKZSDk=
::cRo6pxp7LAbNWATEpCI=
::egkzugNsPRvcWATEpCI=
::dAsiuh18IRvcCxnZtBJQ
::cRYluBh/LU+EWAnk
::YxY4rhs+aU+JeA==
::cxY6rQJ7JhzQF1fEqQJQ
::ZQ05rAF9IBncCkqN+0xwdVs0
::ZQ05rAF9IAHYFVzEqQJQ
::eg0/rx1wNQPfEVWB+kM9LVsJDGQ=
::fBEirQZwNQPfEVWB+kM9LVsJDGQ=
::cRolqwZ3JBvQF1fEqQJQ
::dhA7uBVwLU+EWDk=
::YQ03rBFzNR3SWATElA==
::dhAmsQZ3MwfNWATElA==
::ZQ0/vhVqMQ3MEVWAtB9wSA==
::Zg8zqx1/OA3MEVWAtB9wSA==
::dhA7pRFwIByZRRnk
::Zh4grVQjdCuDJFeL9Ul+KxhoQguDNyapAr4g6uH10/mVoXETW+M0a4fn1LuPK/MbpED8cPY=
::YB416Ek+ZG8=
::
::
::978f952a14a936cc963da21a135fa983
::noak shell bat
::auth: NOAK
@setlocal enabledelayedexpansion
@echo off

set workpath=%cd%
set sdkhome=%NOAK_HOME%

::打印信息 
echo work path=%workpath%
echo sdk home=%sdkhome%

cd %workpath%

set cmd=%1

::如果是帮助命�?
if [%cmd%] == [help] (
    goto :show_usage
)
::如果是jit启动命令
if [%cmd%] == [mkj2m] (
    call :sub_jit_launch %2
    goto :end
)
::如果是java工程创建命令 
if [%cmd%] == [mkprj] (
    call :sub_mkprj %2
    goto :end
)
::如果是jit清理命令 
if [%cmd%] == [clean] (
    call :sub_jit_clean
    goto :end
)

echo cmd "%cmd%" is error!
pause

::打印指令使用说明
:show_usage
echo ">>>>>>list of all commands and usage methods<<<<<<"
echo 1. help               : displays all commands usage method .
echo 2. mkj2m [mainclass]  : make a *.j2m file, if mainclass empty use 'NoakApp'
echo 3. mkprj name         : make a empty noak java project .
echo 4. clean              : delete all generated files .
goto :end

:sub_jit_launch
::显示java版本
java -version

if [%sdkhome%] == [] (
    echo the 'NOAK_HOME' path not found!!!
    pause
    goto :end
)

::清理
call :sub_jit_clean
::创建.bin目录
md .bin

set jit=%sdkhome%\bin\jar\noak-jit.jar
set mainclass=%1

::打印信息 
::echo jit path=%jit%
echo target mainclass: '%mainclass%'

if [%mainclass%] == [] (
    ::没有指定入口,使用默认'NoakApp'�?
    set app=NoakApp
    echo Use default mainclass: '!app!'
) else (
	set app=%mainclass%
)

set sources=src/*.java
set libboot=lib/runtime/noak.jar
set libuser=lib/user/*

::启动java编译，源文件必须是UTF-8编码
javac -encoding utf-8 -sourcepath src %sources% -bootclasspath %libboot% -classpath %libuser% -d .bin
::获取库列�?
set libs=;
for /r "lib/user" %%a in ("*.jar") do (
     set libs=!libs!%%a;)
::启动jar包压�?
jar cvfe %app%.jar %app% -C .bin .
::启动jar包转换成j2m�?
java -jar %jit% -v -bp %libboot%%libs% -cp %app%.jar META-INF -o %app%.j2m

echo Convert: '%app%.jar' to '%app%.j2m'

goto :end

::创建java工程
:sub_mkprj

set mkprj=%1
echo app project name=%mkprj%

md %mkprj%
::拷贝java工程模板
xcopy %sdkhome%\bin\src\template\project\ %mkprj%\ /E /H /C /I 
::拷贝运行时jar�?
copy %sdkhome%\jdk\jdk.jar %mkprj%\lib\runtime\noak.jar /B /-Y

set file_in=%mkprj%\.project.in
set file_personal=%mkprj%\.project
(
    for /f "tokens=*" %%i in (%file_in%) do (
        set s=%%i
        set s=!s:template=%mkprj%!
        echo !s!
    )
)>%file_personal%

del /Q %mkprj%\.project.in

goto :end

::删除已生成的文件
:sub_jit_clean
del /Q *.jar 2>nul
rd /S /Q .bin 2>nul
del /Q *.j2m 2>nul
del /Q *.j2m.log 2>nul
del /Q *.j2m.dbg 2>nul 
echo clean finish.
goto :end

::脚本退�?
:end
goto :eof
