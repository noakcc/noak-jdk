package java.io;

import jvm.charset.CharsetCoder;
import jvm.charset.UTF8Decoder;
import jvm.charset.CharsetInputStreamReader;

public class InputStreamReader extends CharsetInputStreamReader {
	private static final int BUFFERSIZE = 32;

	public InputStreamReader(InputStream os) {
		super(os, new UTF8Decoder(), BUFFERSIZE);
	}

	public InputStreamReader(InputStream os, String charset) throws UnsupportedEncodingException {
		super(os, CharsetCoder.getDecoder(charset), BUFFERSIZE);
	}
}
