package java.io;

import jvm.charset.CharsetCoder;
import jvm.charset.UTF8Encoder;
import jvm.charset.CharsetOutputStreamWriter;

public class OutputStreamWriter extends CharsetOutputStreamWriter {
	private static final int BUFFERSIZE = 32;

	public OutputStreamWriter(OutputStream os) {
		// super(os, new UTF8Encoder(), BUFFERSIZE);
		// default UTF-8
		super(os, new UTF8Encoder(), BUFFERSIZE);
	}

	public OutputStreamWriter(OutputStream os, String charset) throws UnsupportedEncodingException {
		super(os, CharsetCoder.getEncoder(charset), BUFFERSIZE);
	}
}
