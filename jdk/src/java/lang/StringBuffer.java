package java.lang;

/**
 * An expandable string of characters. Actually not very expandable! 09/25/2001
 * added number formatting thanks to Martin E. Nielsen. You must ensure that the
 * buffer is large enough to take the formatted number.
 * <P>
 * 
 * @author <a href="mailto:martin@egholm-nielsen.dk">Martin E. Nielsen</a>
 * @author Sven Köhler
 */
public final class StringBuffer implements CharSequence {
	private static final int INITIAL_CAPACITY = 10;
	private static final int CAPACITY_INCREMENT_NUM = 3; // numerator of the increment factor
	private static final int CAPACITY_INCREMENT_DEN = 2; // denominator of the increment factor

	// MISSING codePointAt(int)
	// MISSING codePointBefore(int)
	// MISSING codePointCount(int, int)
	// MISSING offsetByCodePoints(int, int)

	private char[] characters;
	private int curLen = 0;

	public void ensureCapacity(int minCapacity) {
		int cl = characters.length;
		if (cl < minCapacity) {
			cl = cl * CAPACITY_INCREMENT_NUM / CAPACITY_INCREMENT_DEN + 1;
			while (cl < minCapacity)
				cl = cl * CAPACITY_INCREMENT_NUM / CAPACITY_INCREMENT_DEN + 1;

			char[] newData = new char[cl];
			System.arraycopy(characters, 0, newData, 0, curLen);
			characters = newData;
		}
	}

	public StringBuffer() {
		characters = new char[INITIAL_CAPACITY];
	}

	public StringBuffer(CharSequence seq) {
		int len = seq.length();
		curLen = len;
		characters = new char[len];
		for (int i = 0; i < len; i++)
			characters[i] = seq.charAt(i);
	}

	public StringBuffer(int length) {
		if (length < 0)
			throw new NegativeArraySizeException("length is negative");

		characters = new char[length];
	}

	public StringBuffer(String aString) {
		characters = aString.toCharArray();
		curLen = characters.length;
	}

	public synchronized StringBuffer delete(int start, int end) {
		if (start < 0 || start > curLen)
			throw new StringIndexOutOfBoundsException(start);
		if (end < start)
			throw new StringIndexOutOfBoundsException();
		if (end > curLen)
			end = curLen;

		System.arraycopy(characters, end, characters, start, curLen - end);
		curLen -= end - start;

		return this;
	}

	public StringBuffer deleteCharAt(int index) {
		return this.delete(index, index + 1);
	}

	public StringBuffer append(String s) {
		return this.appendInternal(s);
	}

	public StringBuffer append(Object aObject) {
		return this.appendInternal(String.valueOf(aObject));
	}

	public StringBuffer append(boolean aBoolean) {
		return this.appendInternal(String.valueOf(aBoolean));
	}

	public synchronized StringBuffer append(char aChar) {
		int newLen = curLen + 1;
		ensureCapacity(newLen);

		characters[curLen] = aChar;
		curLen = newLen;

		return this;
	}

	public StringBuffer append(char[] c) {
		return this.append(c, 0, c.length);
	}

	public synchronized StringBuffer append(char[] c, int off, int len) {
		int slen = 0;
		for (int i = 0; i < len; i++) {
			if (c[off + i] == 0) {
				break;
			}
			slen++;
		}
		int newLen = curLen + slen;
		ensureCapacity(newLen);

		for (int i = 0; i < slen; i++)
			characters[curLen + i] = c[off + i];
		curLen = newLen;

		return this;
	}

	public StringBuffer append(CharSequence cs) {
		return this.append(cs, 0, cs.length());
	}

	public synchronized StringBuffer append(StringBuffer sb) {
		int len = sb.length();
		this.ensureCapacity(curLen + len);
		sb.getChars(0, len, characters, curLen);
		return this;
	}

	public synchronized StringBuffer append(CharSequence cs, int start, int end) {
		int len = end - start;
		int newLen = curLen + len;
		ensureCapacity(newLen);

		for (int i = 0; i < len; i++)
			characters[curLen + i] = cs.charAt(start + i);
		curLen = newLen;

		return this;
	}

	public synchronized StringBuffer append(int i) {
		int intLen = StringUtils.exactStringLength(i, 10);
		int newLen = curLen + intLen;
		ensureCapacity(newLen);

		StringUtils.getIntChars(characters, newLen, i, 10);
		curLen = newLen;

		return this;
	}

	public synchronized StringBuffer append(long aLong) {
		int intLen = StringUtils.exactStringLength(aLong, 10);
		int newLen = curLen + intLen;
		ensureCapacity(newLen);

		StringUtils.getLongChars(characters, newLen, aLong, 10);
		curLen = newLen;

		return this;
	}

	public synchronized StringBuffer append(float aFloat) {
		ensureCapacity(curLen + StringUtils.MAX_FLOAT_CHARS);
		curLen = StringUtils.getFloatChars(aFloat, characters, curLen);
		return this;
	}

	public synchronized StringBuffer append(double aDouble) {
		ensureCapacity(curLen + StringUtils.MAX_DOUBLE_CHARS);
		curLen = StringUtils.getDoubleChars(aDouble, characters, curLen);
		return this;
	}

	public synchronized StringBuffer appendCodePoint(int cp) {
		ensureCapacity(curLen + 2);
		curLen += Character.toChars(cp, characters, curLen);
		return this;
	}

	/**
	 * Appends a string with no null checking
	 */
	private StringBuffer appendInternal(String s) {
		if (s == null)
			s = "null";

		// Reminder: compact code more important than speed
		char[] sc = s.characters;
		int sl = sc.length;

		int newlen = curLen + sl;
		this.ensureCapacity(newlen);

		System.arraycopy(sc, 0, characters, curLen, sl);
		curLen = newlen;

		return this;
	}

	public int capacity() {
		return characters.length;
	}

	public int indexOf(String str) {
		return indexOf(str, 0);
	}

	public synchronized int indexOf(String str, int fromIndex) {
		return String.indexOf(characters, 0, curLen, str.characters, 0, str.characters.length, fromIndex);
	}

	public synchronized int lastIndexOf(String str) {
		// Note, synchronization achieved via other invocations
		return lastIndexOf(str, curLen);
	}

	public synchronized int lastIndexOf(String str, int fromIndex) {
		return String.lastIndexOf(characters, 0, curLen, str.characters, 0, str.characters.length, fromIndex);
	}

	@Override
	public synchronized String toString() {
		return new String(characters, 0, curLen);
	}

	@Override
	public synchronized char charAt(int i) {
		if (i < 0 || i >= curLen)
			throw new StringIndexOutOfBoundsException(i);

		return characters[i];
	}

	public synchronized void setCharAt(int i, char ch) {
		if (i < 0 || i >= curLen)
			throw new StringIndexOutOfBoundsException(i);

		characters[i] = ch;
	}

	public synchronized void setLength(int newLen) {
		if (newLen < 0)
			throw new IndexOutOfBoundsException();

		ensureCapacity(newLen);
		for (int i = curLen; i < newLen; i++)
			characters[i] = 0;
		curLen = newLen;
	}

	@Override
	public synchronized int length() {
		return curLen;
	}

	/**
	 * Retrieves the contents of the StringBuilder in the form of an array of
	 * characters.
	 */
	public synchronized void getChars(int start, int end, char[] dst, int dstStart) {
		if (end > curLen)
			throw new StringIndexOutOfBoundsException(end);
		System.arraycopy(characters, start, dst, dstStart, end - start);
	}

	public String substring(int start) {
		return substring(start, curLen);
	}

	public synchronized String substring(int start, int end) {
		if (start < 0 || start > curLen)
			throw new StringIndexOutOfBoundsException(start);
		if (end > curLen)
			throw new StringIndexOutOfBoundsException(end);
		if (end < start)
			throw new StringIndexOutOfBoundsException(end - start);

		int len = end - start;
		return new String(characters, start, len);
	}

	@Override
	public CharSequence subSequence(int start, int end) {
		return substring(start, end);
	}

	public void trimToSize() {
		char[] tmp = new char[curLen];
		System.arraycopy(characters, 0, tmp, 0, curLen);
		characters = tmp;
	}

	/**
	 * Replace characters between index <code>start</code> (inclusive) and
	 * <code>end</code> (exclusive) with <code>str</code>. If <code>end</code> is
	 * larger than the size of this StringBuffer, all characters after
	 * <code>start</code> are replaced.
	 *
	 * @param start the beginning index of characters to delete (inclusive)
	 * @param end   the ending index of characters to delete (exclusive)
	 * @param str   the new <code>String</code> to insert
	 * @return this <code>StringBuffer</code>
	 * @throws StringIndexOutOfBoundsException if start or end are out of bounds
	 * @throws NullPointerException            if str is null
	 * @since 1.2
	 */
	public StringBuffer replace(int start, int end, String str) {
		if (start < 0 || start > curLen || start > end)
			throw new StringIndexOutOfBoundsException(start);

		int len = str.length();
		// Calculate the difference in 'count' after the replace.
		int delta = len - (end > curLen ? curLen : end) + start;
		ensureCapacity(curLen + delta);

		if (delta != 0 && end < curLen)
			System.arraycopy(characters, end, characters, end + delta, curLen - end);

		str.getChars(0, len, characters, start);
		curLen += delta;
		return this;
	}

	/**
	 * Insert a subarray of the <code>char[]</code> argument into this
	 * <code>StringBuffer</code>.
	 *
	 * @param offset     the place to insert in this buffer
	 * @param str        the <code>char[]</code> to insert
	 * @param str_offset the index in <code>str</code> to start inserting from
	 * @param len        the number of characters to insert
	 * @return this <code>StringBuffer</code>
	 * @throws NullPointerException            if <code>str</code> is
	 *                                         <code>null</code>
	 * @throws StringIndexOutOfBoundsException if any index is out of bounds
	 * @since 1.2
	 */
	public StringBuffer insert(int offset, char[] str, int str_offset, int len) {
		if (offset < 0 || offset > curLen || len < 0 || str_offset < 0 || str_offset > str.length - len)
			throw new StringIndexOutOfBoundsException();
		int slen = 0;
		for (int i = 0; i < len; i++) {
			if (str[str_offset + i] == 0) {
				break;
			}
			slen++;
		}
		ensureCapacity(curLen + slen);
		System.arraycopy(characters, offset, characters, offset + slen, curLen - offset);
		System.arraycopy(str, str_offset, characters, offset, slen);
		curLen += slen;

		return this;
	}

	/**
	 * Insert the <code>char[]</code> argument into this <code>StringBuffer</code>.
	 *
	 * @param offset the place to insert in this buffer
	 * @param data   the <code>char[]</code> to insert
	 * @return this <code>StringBuffer</code>
	 * @throws NullPointerException            if <code>data</code> is
	 *                                         <code>null</code>
	 * @throws StringIndexOutOfBoundsException if offset is out of bounds
	 * @see #insert(int, char[], int, int)
	 */
	public StringBuffer insert(int offset, char[] data) {
		return insert(offset, data, 0, data.length);
	}

	/**
	 * Insert the <code>String</code> value of the argument into this
	 * <code>StringBuffer</code>. Uses <code>String.valueOf()</code> to convert to
	 * <code>String</code>.
	 *
	 * @param offset the place to insert in this buffer
	 * @param obj    the <code>Object</code> to convert and insert
	 * @return this <code>StringBuffer</code>
	 * @exception StringIndexOutOfBoundsException if offset is out of bounds
	 * @see String#valueOf(Object)
	 */
	public StringBuffer insert(int offset, Object obj) {
		return insert(offset, obj == null ? "null" : obj.toString());
	}

	/**
	 * Insert the <code>String</code> argument into this <code>StringBuffer</code>.
	 * If str is null, the String "null" is used instead.
	 *
	 * @param offset the place to insert in this buffer
	 * @param str    the <code>String</code> to insert
	 * @return this <code>StringBuffer</code>
	 * @throws StringIndexOutOfBoundsException if offset is out of bounds
	 */
	public StringBuffer insert(int offset, String str) {
		if (offset < 0 || offset > curLen)
			throw new StringIndexOutOfBoundsException(offset);
		if (str == null)
			str = "null";
		int len = str.length();
		ensureCapacity(curLen + len);
		System.arraycopy(characters, offset, characters, offset + len, curLen - offset);
		str.getChars(0, len, characters, offset);
		curLen += len;
		return this;
	}

	/**
	 * Reverse the characters in this StringBuffer. The same sequence of characters
	 * exists, but in the reverse index ordering.
	 *
	 * @return this <code>StringBuffer</code>
	 */
	public StringBuffer reverse() {
		// Call ensureCapacity to enforce copy-on-write.
		ensureCapacity(curLen);
		for (int i = curLen >> 1, j = curLen - i; --i >= 0; ++j) {
			char c = characters[i];
			characters[i] = characters[j];
			characters[j] = c;
		}
		return this;
	}

}
