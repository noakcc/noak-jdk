package java.lang;

import java.io.InputStream;
import java.io.PrintStream;

import jvm.core.VM;
import jvm.host.*;

/**
 * System utilities.
 */
public final class System {
	private System() {
	}

	/**
	 * Copies one array to another.
	 */
	public static native void arraycopy(Object src, int srcOffset, Object dest, int destOffset, int length);

	/**
	 * Terminate the application.
	 */
	public static void exit(int code) {
		Runtime.getRuntime().exit(code);
	}

	/**
	 * Current time expressed in milliseconds. In the JVM, this is the number of
	 * milliseconds since the JVM has been on. (In Java, this would be since January
	 * 1st, 1970).
	 */
	public static long currentTimeMillis() {
		return VM.currUTCMillSeconds();
	}

	/**
	 * Returns <code>property string</code>. In this JVM, there are some system
	 * properties.
	 * 
	 * @param name name of the system property
	 * @return <code>property string</code>
	 */
	public static String getProperty(String name) {
		switch (name) {
		case "line.separator":
			return VM.getLineSeparator();
		default:
			break;
		}
		return null;
	}

	/**
	 * Returns <code>def</code>. In JMV, there are no system properties. So this
	 * function returns <code>def</code>.
	 * 
	 * @param name name of the system property
	 * @param def  default value that is returns if system property doesn't exist
	 * @return <code>def</code>
	 */
	public static String getProperty(String name, String def) {
		String prop = getProperty(name);
		if(prop == null) {
			return def;
		}
		return prop;
	}

	/**
	 * Get the singleton instance of Runtime.
	 */
	public static Runtime getRuntime() {
		return Runtime.getRuntime();
	}

	public static int identityHashCode(Object obj) {
		return System.getDataAddress(obj);
	}

	private native static int getDataAddress(Object obj);

	/**
	 * Collect garbage
	 */
	public static native void gc();

	public static InputStream in = new TerminalInputStream();
	public static PrintStream out = new PrintStream(new TerminalOutputStream());

	/**
	 * Redirect System.in
	 * 
	 * @param in a InputStream
	 */
	public static void setIn(InputStream in) {
		System.in = in;
	}

	/**
	 * Redirect System.out
	 * 
	 * @param out a PrintStream
	 */
	public static void setOut(PrintStream out) {
		System.out = out;
	}

	public static PrintStream err = out;

	/**
	 * Redirect System.err
	 * 
	 * @param err a PrintStream
	 */
	public static void setErr(PrintStream err) {
		System.err = err;
	}

}
