package java.lang.annotation;

public interface Annotation {
	Class<? extends Annotation> annotationType();

	@Override
	boolean equals(Object obj);

	@Override
	int hashCode();

	@Override
	String toString();
}
