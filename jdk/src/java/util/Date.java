/* java.util.Date
   Copyright (C) 1998, 1999, 2000, 2001, 2005  Free Software Foundation, Inc.

This file is part of GNU Classpath.

GNU Classpath is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

GNU Classpath is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU Classpath; see the file COPYING.  If not, write to the
Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301 USA.

Linking this library statically or dynamically with other modules is
making a combined work based on this library.  Thus, the terms and
conditions of the GNU General Public License cover the whole
combination.

As a special exception, the copyright holders of this library give you
permission to link this library with independent modules to produce an
executable, regardless of the license terms of these independent
modules, and to copy and distribute the resulting executable under
terms of your choice, provided that you also meet, for each linked
independent module, the terms and conditions of the license of that
module.  An independent module is a module which is not derived from
or based on this library.  If you modify this library, you may extend
this exception to your version of the library, but you are not
obligated to do so.  If you do not wish to do so, delete this
exception statement from your version. */

package java.util;

import java.io.Serializable;

import jvm.core.VM;

/**
 * <p>
 * This class represents a specific time in milliseconds since the epoch. The
 * epoch is 1970, January 1 00:00:00.0000 UTC.
 * </p>
 * <p>
 * <code>Date</code> is intended to reflect universal time coordinate (UTC), but
 * this depends on the underlying host environment. Most operating systems don't
 * handle the leap second, which occurs about once every year or so. The leap
 * second is added to the last minute of the day on either the 30th of June or
 * the 31st of December, creating a minute 61 seconds in length.
 * </p>
 * <p>
 * The representations of the date fields are as follows:
 * <ul>
 * <li>Years are specified as the difference between the year and 1900. Thus,
 * the final year used is equal to 1900 + y, where y is the input value.</li>
 * <li>Months are represented using zero-based indexing, making 0 January and 11
 * December.</li>
 * <li>Dates are represented with the usual values of 1 through to 31.</li>
 * <li>Hours are represented in the twenty-four hour clock, with integer values
 * from 0 to 23. 12am is 0, and 12pm is 12.</li>
 * <li>Minutes are again as usual, with values from 0 to 59.</li>
 * <li>Seconds are represented with the values 0 through to 61, with 60 and 61
 * being leap seconds (as per the ISO C standard).</li>
 * </ul>
 * </p>
 * <p>
 * Prior to JDK 1.1, this class was the sole class handling date and time
 * related functionality. However, this particular solution was not amenable to
 * internationalization. The new <code>Calendar</code> class should now be used
 * to handle dates and times, with <code>Date</code> being used only for values
 * in milliseconds since the epoch. The <code>Calendar</code> class, and its
 * concrete implementations, handle the interpretation of these values into
 * minutes, hours, days, months and years. The formatting and parsing of dates
 * is left to the <code>DateFormat</code> class, which is able to handle the
 * different types of date format which occur in different locales.
 * </p>
 *
 * @see Calendar
 * @see GregorianCalendar
 * @author Jochen Hoenicke
 * @author Per Bothner (bothner@cygnus.com)
 * @author Andrew John Hughes (gnu_andrew@member.fsf.org)
 */
public class Date implements Cloneable, Comparable<Date>, Serializable {
	/**
	 * The time in milliseconds since the epoch.
	 */
	private transient long time;

	/**
	 * An array of week names used to map names to integer values.
	 */
	private static final String[] weekNames = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
	/**
	 * An array of month names used to map names to integer values.
	 */
	private static final String[] monthNames = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct",
			"Nov", "Dec" };

	/**
	 * Creates a new Date Object representing the current time.
	 */
	public Date() {
		time = VM.currUTCMillSeconds();
	}

	/**
	 * Creates a new Date Object representing the given time.
	 *
	 * @param time the time in milliseconds since the epoch.
	 */
	public Date(long time) {
		this.time = time;
	}

	/**
	 * Creates a new Date Object representing the given time.
	 *
	 * @deprecated use <code>new GregorianCalendar(year+1900, month,
	 * day)</code> instead.
	 * @param year  the difference between the required year and 1900.
	 * @param month the month as a value between 0 and 11.
	 * @param day   the day as a value between 0 and 31.
	 */
	@Deprecated
	public Date(int year, int month, int day) {
		this(year, month, day, 0, 0, 0);
	}

	/**
	 * Creates a new Date Object representing the given time.
	 *
	 * @deprecated use <code>new GregorianCalendar(year+1900, month,
	 * day, hour, min)</code> instead.
	 * @param year  the difference between the required year and 1900.
	 * @param month the month as a value between 0 and 11.
	 * @param day   the day as a value between 0 and 31.
	 * @param hour  the hour as a value between 0 and 23, in 24-hour clock notation.
	 * @param min   the minute as a value between 0 and 59.
	 */
	@Deprecated
	public Date(int year, int month, int day, int hour, int min) {
		this(year, month, day, hour, min, 0);
	}

	/**
	 * Creates a new Date Object representing the given time.
	 *
	 * @deprecated use <code>new GregorianCalendar(year+1900, month,
	 * day, hour, min, sec)</code> instead.
	 * @param year  the difference between the required year and 1900.
	 * @param month the month as a value between 0 and 11.
	 * @param day   the day as a value between 0 and 31.
	 * @param hour  the hour as a value between 0 and 23, in 24-hour clock notation.
	 * @param min   the minute as a value between 0 and 59.
	 * @param sec   the second as a value between 0 and 61 (with 60 and 61 being
	 *              leap seconds).
	 */
	@Deprecated
	public Date(int year, int month, int day, int hour, int min, int sec) {
		GregorianCalendar cal = new GregorianCalendar(year + 1900, month, day, hour, min, sec);
		time = cal.getTimeInMillis();
	}

	/**
	 * Gets the time represented by this object.
	 *
	 * @return the time in milliseconds since the epoch.
	 */
	public long getTime() {
		return time;
	}

	/**
	 * Sets the time which this object should represent.
	 *
	 * @param time the time in milliseconds since the epoch.
	 */
	public void setTime(long time) {
		this.time = time;
	}

	/**
	 * Tests if this date is after the specified date.
	 *
	 * @param when the other date
	 * @return true, if the date represented by this object is strictly later than
	 *         the time represented by when.
	 */
	public boolean after(Date when) {
		return time > when.time;
	}

	/**
	 * Tests if this date is before the specified date.
	 *
	 * @param when the other date
	 * @return true, if the date represented by when is strictly later than the time
	 *         represented by this object.
	 */
	public boolean before(Date when) {
		return time < when.time;
	}

	/**
	 * Compares two dates for equality.
	 *
	 * @param obj the object to compare.
	 * @return true, if obj is a Date object and the time represented by obj is
	 *         exactly the same as the time represented by this object.
	 */
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof Date && time == ((Date) obj).time);
	}

	/**
	 * Compares two dates.
	 *
	 * @param when the other date.
	 * @return 0, if the date represented by obj is exactly the same as the time
	 *         represented by this object, a negative if this Date is before the
	 *         other Date, and a positive value otherwise.
	 */
	@Override
	public int compareTo(Date when) {
		return (time < when.time) ? -1 : (time == when.time) ? 0 : 1;
	}

	/**
	 * Computes the hash code of this <code>Date</code> as the XOR of the most
	 * significant and the least significant 32 bits of the 64 bit milliseconds
	 * value.
	 *
	 * @return the hash code.
	 */
	@Override
	public int hashCode() {
		return (int) time ^ (int) (time >>> 32);
	}

	/**
	 * <p>
	 * Returns a string representation of this date using the following date format:
	 * </p>
	 * <p>
	 * <code>day mon dd hh:mm:ss zz yyyy</code>
	 * </p>
	 * <p>
	 * where the fields used here are:
	 * <ul>
	 * <li><code>day</code> -- the day of the week (Sunday through to Saturday).
	 * </li>
	 * <li><code>mon</code> -- the month (Jan to Dec).</li>
	 * <li><code>dd</code> -- the day of the month as two decimal digits (01 to 31).
	 * </li>
	 * <li><code>hh</code> -- the hour of the day as two decimal digits in 24-hour
	 * clock notation (01 to 23).</li>
	 * <li><code>mm</code> -- the minute of the day as two decimal digits (01 to
	 * 59).</li>
	 * <li><code>ss</code> -- the second of the day as two decimal digits (01 to
	 * 61).</li>
	 * <li><code>zz</code> -- the time zone information if available. The possible
	 * time zones used include the abbreviations recognised by <code>parse()</code>
	 * (e.g. GMT, CET, etc.) and may reflect the fact that daylight savings time is
	 * in effect. The empty string is used if there is no time zone information.
	 * </li>
	 * <li><code>yyyy</code> -- the year as four decimal digits.</li>
	 * </ul>
	 * <p>
	 * The <code>DateFormat</code> class should now be preferred over using this
	 * method.
	 * </p>
	 *
	 * @return A string of the form 'day mon dd hh:mm:ss zz yyyy'
	 * @see #parse(String)
	 */
	@Override
	public String toString() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		String day = "0" + cal.get(Calendar.DATE);
		String hour = "0" + cal.get(Calendar.HOUR_OF_DAY);
		String min = "0" + cal.get(Calendar.MINUTE);
		String sec = "0" + cal.get(Calendar.SECOND);
		String year = "000" + cal.get(Calendar.YEAR);
		return weekNames[cal.get(Calendar.DAY_OF_WEEK) - 1] + " " + monthNames[cal.get(Calendar.MONTH)] + " "
				+ day.substring(day.length() - 2) + " " + hour.substring(hour.length() - 2) + ":"
				+ min.substring(min.length() - 2) + ":" + sec.substring(sec.length() - 2) + " "
				+ year.substring(year.length() - 4);
	}

	/**
	 * Returns the difference between the year represented by this <code>Date</code>
	 * object and 1900.
	 *
	 * @return the year minus 1900 represented by this date object.
	 * @deprecated Use Calendar instead of Date, and use get(Calendar.YEAR) instead.
	 *             Note the 1900 difference in the year.
	 * @see Calendar
	 * @see #setYear(int)
	 */
	@Deprecated
	public int getYear() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		return cal.get(Calendar.YEAR) - 1900;
	}

	/**
	 * Sets the year to the specified year, plus 1900. The other fields are only
	 * altered as required to match the same date and time in the new year. Usually,
	 * this will mean that the fields are not changed at all, but in the case of a
	 * leap day or leap second, the fields will change in relation to the existence
	 * of such an event in the new year. For example, if the date specifies February
	 * the 29th, 2000, then this will become March the 1st if the year is changed to
	 * 2001, as 2001 is not a leap year. Similarly, a seconds value of 60 or 61 may
	 * result in the seconds becoming 0 and the minute increasing by 1, if the new
	 * time does not include a leap second.
	 *
	 * @param year the year minus 1900.
	 * @deprecated Use Calendar instead of Date, and use set(Calendar.YEAR, year)
	 *             instead. Note about the 1900 difference in year.
	 * @see #getYear()
	 * @see Calendar
	 */
	@Deprecated
	public void setYear(int year) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		cal.set(Calendar.YEAR, 1900 + year);
		time = cal.getTimeInMillis();
	}

	/**
	 * Returns the month represented by this <code>Date</code> object, as a value
	 * between 0 (January) and 11 (December).
	 *
	 * @return the month represented by this date object (zero based).
	 * @deprecated Use Calendar instead of Date, and use get(Calendar.MONTH)
	 *             instead.
	 * @see #setMonth(int)
	 * @see Calendar
	 */
	@Deprecated
	public int getMonth() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		return cal.get(Calendar.MONTH);
	}

	/**
	 * Sets the month to the given value. The other fields are only altered as
	 * necessary to match the same date and time in the new month. In most cases,
	 * the other fields won't change at all. However, in the case of a shorter month
	 * or a leap second, values may be adjusted. For example, if the day of the
	 * month is currently 31, and the month value is changed from January (0) to
	 * September (8), the date will become October the 1st, as September only has 30
	 * days. Similarly, a seconds value of 60 or 61 (a leap second) may result in
	 * the seconds value being reset to 0 and the minutes value being incremented by
	 * 1, if the new time does not include a leap second.
	 *
	 * @param month the month, with a zero-based index from January.
	 * @deprecated Use Calendar instead of Date, and use set(Calendar.MONTH, month)
	 *             instead.
	 * @see #getMonth()
	 * @see Calendar
	 */
	@Deprecated
	public void setMonth(int month) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		cal.set(Calendar.MONTH, month);
		time = cal.getTimeInMillis();
	}

	/**
	 * Returns the day of the month of this <code>Date</code> object, as a value
	 * between 0 and 31.
	 *
	 * @return the day of month represented by this date object.
	 * @deprecated Use Calendar instead of Date, and use get(Calendar.DATE) instead.
	 * @see Calendar
	 * @see #setDate(int)
	 */
	@Deprecated
	public int getDate() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		return cal.get(Calendar.DATE);
	}

	/**
	 * Sets the date to the given value. The other fields are only altered as
	 * necessary to match the same date and time on the new day of the month. In
	 * most cases, the other fields won't change at all. However, in the case of a
	 * leap second or the day being out of the range of the current month, values
	 * may be adjusted. For example, if the day of the month is currently 30 and the
	 * month is June, a new day of the month value of 31 will cause the month to
	 * change to July, as June only has 30 days . Similarly, a seconds value of 60
	 * or 61 (a leap second) may result in the seconds value being reset to 0 and
	 * the minutes value being incremented by 1, if the new time does not include a
	 * leap second.
	 *
	 * @param date the date.
	 * @deprecated Use Calendar instead of Date, and use set(Calendar.DATE, date)
	 *             instead.
	 * @see Calendar
	 * @see #getDate()
	 */
	@Deprecated
	public void setDate(int date) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		cal.set(Calendar.DATE, date);
		time = cal.getTimeInMillis();
	}

	/**
	 * Returns the day represented by this <code>Date</code> object as an integer
	 * between 0 (Sunday) and 6 (Saturday).
	 *
	 * @return the day represented by this date object.
	 * @deprecated Use Calendar instead of Date, and use get(Calendar.DAY_OF_WEEK)
	 *             instead.
	 * @see Calendar
	 */
	@Deprecated
	public int getDay() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		// For Calendar, Sunday is 1. For Date, Sunday is 0.
		return cal.get(Calendar.DAY_OF_WEEK) - 1;
	}

	/**
	 * Returns the hours represented by this <code>Date</code> object as an integer
	 * between 0 and 23.
	 *
	 * @return the hours represented by this date object.
	 * @deprecated Use Calendar instead of Date, and use get(Calendar.HOUR_OF_DAY)
	 *             instead.
	 * @see Calendar
	 * @see #setHours(int)
	 */
	@Deprecated
	public int getHours() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		return cal.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * Sets the hours to the given value. The other fields are only altered as
	 * necessary to match the same date and time in the new hour. In most cases, the
	 * other fields won't change at all. However, in the case of a leap second,
	 * values may be adjusted. For example, a seconds value of 60 or 61 (a leap
	 * second) may result in the seconds value being reset to 0 and the minutes
	 * value being incremented by 1 if the new hour does not contain a leap second.
	 *
	 * @param hours the hours.
	 * @deprecated Use Calendar instead of Date, and use set(Calendar.HOUR_OF_DAY,
	 *             hours) instead.
	 * @see Calendar
	 * @see #getHours()
	 */
	@Deprecated
	public void setHours(int hours) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		cal.set(Calendar.HOUR_OF_DAY, hours);
		time = cal.getTimeInMillis();
	}

	/**
	 * Returns the number of minutes represented by the <code>Date</code> object, as
	 * an integer between 0 and 59.
	 *
	 * @return the minutes represented by this date object.
	 * @deprecated Use Calendar instead of Date, and use get(Calendar.MINUTE)
	 *             instead.
	 * @see Calendar
	 * @see #setMinutes(int)
	 */
	@Deprecated
	public int getMinutes() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		return cal.get(Calendar.MINUTE);
	}

	/**
	 * Sets the minutes to the given value. The other fields are only altered as
	 * necessary to match the same date and time in the new minute. In most cases,
	 * the other fields won't change at all. However, in the case of a leap second,
	 * values may be adjusted. For example, a seconds value of 60 or 61 (a leap
	 * second) may result in the seconds value being reset to 0 and the minutes
	 * value being incremented by 1 if the new minute does not contain a leap
	 * second.
	 *
	 * @param minutes the minutes.
	 * @deprecated Use Calendar instead of Date, and use set(Calendar.MINUTE,
	 *             minutes) instead.
	 * @see Calendar
	 * @see #getMinutes()
	 */
	@Deprecated
	public void setMinutes(int minutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		cal.set(Calendar.MINUTE, minutes);
		time = cal.getTimeInMillis();
	}

	/**
	 * Returns the number of seconds represented by the <code>Date</code> object, as
	 * an integer between 0 and 61 (60 and 61 being leap seconds).
	 *
	 * @return the seconds represented by this date object.
	 * @deprecated Use Calendar instead of Date, and use get(Calendar.SECOND)
	 *             instead.
	 * @see Calendar
	 * @see #setSeconds(int)
	 */
	@Deprecated
	public int getSeconds() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		return cal.get(Calendar.SECOND);
	}

	/**
	 * Sets the seconds to the given value. The other fields are only altered as
	 * necessary to match the same date and time in the new minute. In most cases,
	 * the other fields won't change at all. However, in the case of a leap second,
	 * values may be adjusted. For example, setting the seconds value to 60 or 61 (a
	 * leap second) may result in the seconds value being reset to 0 and the minutes
	 * value being incremented by 1, if the current time does not contain a leap
	 * second.
	 *
	 * @param seconds the seconds.
	 * @deprecated Use Calendar instead of Date, and use set(Calendar.SECOND,
	 *             seconds) instead.
	 * @see Calendar
	 * @see #getSeconds()
	 */
	@Deprecated
	public void setSeconds(int seconds) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		cal.set(Calendar.SECOND, seconds);
		time = cal.getTimeInMillis();
	}

}
