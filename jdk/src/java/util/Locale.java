/* Locale.java -- i18n locales
   Copyright (C) 1998, 1999, 2001, 2002, 2005, 2006  Free Software Foundation, Inc.

This file is part of GNU Classpath.

GNU Classpath is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

GNU Classpath is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU Classpath; see the file COPYING.  If not, write to the
Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301 USA.

Linking this library statically or dynamically with other modules is
making a combined work based on this library.  Thus, the terms and
conditions of the GNU General Public License cover the whole
combination.

As a special exception, the copyright holders of this library give you
permission to link this library with independent modules to produce an
executable, regardless of the license terms of these independent
modules, and to copy and distribute the resulting executable under
terms of your choice, provided that you also meet, for each linked
independent module, the terms and conditions of the license of that
module.  An independent module is a module which is not derived from
or based on this library.  If you modify this library, you may extend
this exception to your version of the library, but you are not
obligated to do so.  If you do not wish to do so, delete this
exception statement from your version. */

package java.util;

import java.io.Serializable;

/**
 * Locales represent a specific country and culture. Classes which can be passed
 * a Locale object tailor their information for a given locale. For instance,
 * currency number formatting is handled differently for the USA and France.
 *
 * <p>
 * Locales are made up of a language code, a country code, and an optional set
 * of variant strings. Language codes are represented by
 * <a href="http://www.ics.uci.edu/pub/ietf/http/related/iso639.txt"> ISO
 * 639:1988</a> w/ additions from ISO 639/RA Newsletter No. 1/1989 and a
 * decision of the Advisory Committee of ISO/TC39 on August 8, 1997.
 *
 * <p>
 * Country codes are represented by
 * <a href="http://www.chemie.fu-berlin.de/diverse/doc/ISO_3166.html"> ISO
 * 3166</a>. Variant strings are vendor and browser specific. Standard variant
 * strings include "POSIX" for POSIX, "WIN" for MS-Windows, and "MAC" for
 * Macintosh. When there is more than one variant string, they must be separated
 * by an underscore (U+005F).
 *
 * <p>
 * The default locale is determined by the values of the system properties
 * user.language, user.country (or user.region), and user.variant, defaulting to
 * "en_US". Note that the locale does NOT contain the conversion and formatting
 * capabilities (for that, use ResourceBundle and java.text). Rather, it is an
 * immutable tag object for identifying a given locale, which is referenced by
 * these other classes when they must make locale-dependent decisions.
 *
 * @see ResourceBundle
 * @see java.text.Format
 * @see java.text.NumberFormat
 * @see java.text.Collator
 * @author Jochen Hoenicke
 * @author Paul Fisher
 * @author Eric Blake (ebb9@email.byu.edu)
 * @author Andrew John Hughes (gnu_andrew@member.fsf.org)
 * @since 1.1
 * @status updated to 1.4
 */
public final class Locale implements Serializable, Cloneable {
	/** Locale which represents the United States. */
	public static final Locale US = getLocale("en", "US");

	/**
	 * The root locale, used as the base case in lookups by locale-sensitive
	 * operations.
	 */
	public static final Locale ROOT = new Locale("", "", "");

	/**
	 * The language code, as returned by getLanguage().
	 *
	 * @serial the languange, possibly ""
	 */
	private final String language;

	/**
	 * The country code, as returned by getCountry().
	 *
	 * @serial the country, possibly ""
	 */
	private final String country;

	/**
	 * The variant code, as returned by getVariant().
	 *
	 * @serial the variant, possibly ""
	 */
	private final String variant;

	/**
	 * Locale cache. Only created locale objects are stored. Contains all supported
	 * locales when getAvailableLocales() got called.
	 */
	private static transient HashMap localeMap;

	/**
	 * The default locale. Except for during bootstrapping, this should never be
	 * null. Note the logic in the main constructor, to detect when bootstrapping
	 * has completed.
	 */
	private static Locale defaultLocale;

	static {
		String language = "en";
		String country = "US";
		String region = null;
		String variant = "";

		defaultLocale = getLocale(language, (region != null) ? region : country, variant);
	}

	/**
	 * Retrieves the locale with the specified language and country from the cache.
	 *
	 * @param language the language of the locale to retrieve.
	 * @param country  the country of the locale to retrieve.
	 * @return the locale.
	 */
	private static Locale getLocale(String language, String country) {
		return getLocale(language, country, "");
	}

	/**
	 * Retrieves the locale with the specified language, country and variant from
	 * the cache.
	 *
	 * @param language the language of the locale to retrieve.
	 * @param country  the country of the locale to retrieve.
	 * @param variant  the variant of the locale to retrieve.
	 * @return the locale.
	 */
	private static Locale getLocale(String language, String country, String variant) {
		if (localeMap == null)
			localeMap = new HashMap(4);

		String name = language + "_" + country + "_" + variant;
		Locale locale = (Locale) localeMap.get(name);

		if (locale == null) {
			locale = new Locale(language, country, variant);
			localeMap.put(name, locale);
		}

		return locale;
	}

	/**
	 * Convert new iso639 codes to the old ones.
	 *
	 * @param language the language to check
	 * @return the appropriate code
	 */
	private String convertLanguage(String language) {
		if (language.equals(""))
			return language;
		language = language.toLowerCase();
		int index = "he,id,yi".indexOf(language);
		if (index != -1)
			return "iw,in,ji".substring(index, index + 2);
		return language;
	}

	/**
	 * Creates a new locale for the given language and country.
	 *
	 * @param language lowercase two-letter ISO-639 A2 language code
	 * @param country  uppercase two-letter ISO-3166 A2 contry code
	 * @param variant  vendor and browser specific
	 * @throws NullPointerException if any argument is null
	 */
	public Locale(String language, String country, String variant) {
		// During bootstrap, we already know the strings being passed in are
		// the correct capitalization, and not null. We can't call
		// String.toUpperCase during this time, since that depends on the
		// default locale.
		if (defaultLocale != null) {
			language = convertLanguage(language);
			country = country.toUpperCase();
		}
		this.language = language;
		this.country = country;
		this.variant = variant;
	}

	/**
	 * Creates a new locale for the given language and country.
	 *
	 * @param language lowercase two-letter ISO-639 A2 language code
	 * @param country  uppercase two-letter ISO-3166 A2 country code
	 * @throws NullPointerException if either argument is null
	 */
	public Locale(String language, String country) {
		this(language, country, "");
	}

	/**
	 * Creates a new locale for a language.
	 *
	 * @param language lowercase two-letter ISO-639 A2 language code
	 * @throws NullPointerException if either argument is null
	 * @since 1.4
	 */
	public Locale(String language) {
		this(language, "", "");
	}

	/**
	 * Returns the default Locale. The default locale is generally once set on start
	 * up and then never changed. Normally you should use this locale for everywhere
	 * you need a locale. The initial setting matches the default locale, the user
	 * has chosen.
	 *
	 * @return the default locale for this virtual machine
	 */
	public static Locale getDefault() {
		return defaultLocale;
	}

	/**
	 * Changes the default locale. Normally only called on program start up. Note
	 * that this doesn't change the locale for other programs. This has a security
	 * check, <code>PropertyPermission("user.language", "write")</code>, because of
	 * its potential impact to running code.
	 *
	 * @param newLocale the new default locale
	 * @throws NullPointerException if newLocale is null
	 * @throws SecurityException    if permission is denied
	 */
	public static void setDefault(Locale newLocale) {
		if (newLocale == null)
			throw new NullPointerException();

		defaultLocale = newLocale;
	}

	/**
	 * Returns the list of available locales.
	 *
	 * @return the installed locales
	 */
	public static synchronized Locale[] getAvailableLocales() {
		Locale[] availableLocales = new Locale[] { Locale.US };
		return availableLocales;
	}

	/**
	 * Returns the language code of this locale. Some language codes have changed as
	 * ISO 639 has evolved; this returns the old name, even if you built the locale
	 * with the new one.
	 *
	 * @return language code portion of this locale, or an empty String
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Returns the country code of this locale.
	 *
	 * @return country code portion of this locale, or an empty String
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Returns the variant code of this locale.
	 *
	 * @return the variant code portion of this locale, or an empty String
	 */
	public String getVariant() {
		return variant;
	}

	/**
	 * Gets the string representation of the current locale. This consists of the
	 * language, the country, and the variant, separated by an underscore. The
	 * variant is listed only if there is a language or country. Examples: "en",
	 * "de_DE", "_GB", "en_US_WIN", "de__POSIX", "fr__MAC".
	 *
	 * @return the string representation of this Locale
	 * @see #getDisplayName()
	 */
	@Override
	public String toString() {
		if (language.length() == 0 && country.length() == 0)
			return "";
		else if (country.length() == 0 && variant.length() == 0)
			return language;

		StringBuilder result = new StringBuilder(language);
		result.append('_').append(country);
		if (variant.length() != 0)
			result.append('_').append(variant);
		return result.toString();
	}

	/**
	 * Returns the three-letter ISO language abbrevation of this locale.
	 *
	 * @throws MissingResourceException if the three-letter code is not known
	 */
	public String getISO3Language() {
		// We know all strings are interned so we can use '==' for better performance.
		if (language == "")
			return "";
		int index = ("aa,ab,af,am,ar,as,ay,az,ba,be,bg,bh,bi,bn,bo,br,ca,co,cs,cy,da,"
				+ "de,dz,el,en,eo,es,et,eu,fa,fi,fj,fo,fr,fy,ga,gd,gl,gn,gu,ha,iw,"
				+ "hi,hr,hu,hy,ia,in,ie,ik,in,is,it,iu,iw,ja,ji,jw,ka,kk,kl,km,kn,"
				+ "ko,ks,ku,ky,la,ln,lo,lt,lv,mg,mi,mk,ml,mn,mo,mr,ms,mt,my,na,ne,"
				+ "nl,no,oc,om,or,pa,pl,ps,pt,qu,rm,rn,ro,ru,rw,sa,sd,sg,sh,si,sk,"
				+ "sl,sm,sn,so,sq,sr,ss,st,su,sv,sw,ta,te,tg,th,ti,tk,tl,tn,to,tr,"
				+ "ts,tt,tw,ug,uk,ur,uz,vi,vo,wo,xh,ji,yo,za,zh,zu").indexOf(language);

		if (index % 3 != 0 || language.length() != 2)
			throw new MissingResourceException("Can't find ISO3 language for " + language, "java.util.Locale",
					language);

		// Don't read this aloud. These are the three letter language codes.
		return ("aarabkaframharaasmaymazebakbelbulbihbisbenbodbrecatcoscescymdandeu"
				+ "dzoellengepospaesteusfasfinfijfaofrafrygaigdhglggrngujhauhebhinhrv"
				+ "hunhyeinaindileipkindislitaikuhebjpnyidjawkatkazkalkhmkankorkaskur"
				+ "kirlatlinlaolitlavmlgmrimkdmalmonmolmarmsamltmyanaunepnldnorociorm"
				+ "oripanpolpusporquerohrunronruskinsansndsagsrpsinslkslvsmosnasomsqi"
				+ "srpsswsotsunsweswatamteltgkthatirtuktgltsntonturtsotattwiuigukrurd"
				+ "uzbvievolwolxhoyidyorzhazhozul").substring(index, index + 3);
	}

	/**
	 * Returns the three-letter ISO country abbrevation of the locale.
	 *
	 * @throws MissingResourceException if the three-letter code is not known
	 */
	public String getISO3Country() {
		// We know all strings are interned so we can use '==' for better performance.
		if (country == "")
			return "";
		int index = ("AD,AE,AF,AG,AI,AL,AM,AN,AO,AQ,AR,AS,AT,AU,AW,AZ,BA,BB,BD,BE,BF,"
				+ "BG,BH,BI,BJ,BM,BN,BO,BR,BS,BT,BV,BW,BY,BZ,CA,CC,CF,CG,CH,CI,CK,"
				+ "CL,CM,CN,CO,CR,CU,CV,CX,CY,CZ,DE,DJ,DK,DM,DO,DZ,EC,EE,EG,EH,ER,"
				+ "ES,ET,FI,FJ,FK,FM,FO,FR,FX,GA,GB,GD,GE,GF,GH,GI,GL,GM,GN,GP,GQ,"
				+ "GR,GS,GT,GU,GW,GY,HK,HM,HN,HR,HT,HU,ID,IE,IL,IN,IO,IQ,IR,IS,IT,"
				+ "JM,JO,JP,KE,KG,KH,KI,KM,KN,KP,KR,KW,KY,KZ,LA,LB,LC,LI,LK,LR,LS,"
				+ "LT,LU,LV,LY,MA,MC,MD,MG,MH,MK,ML,MM,MN,MO,MP,MQ,MR,MS,MT,MU,MV,"
				+ "MW,MX,MY,MZ,NA,NC,NE,NF,NG,NI,NL,NO,NP,NR,NU,NZ,OM,PA,PE,PF,PG,"
				+ "PH,PK,PL,PM,PN,PR,PT,PW,PY,QA,RE,RO,RU,RW,SA,SB,SC,SD,SE,SG,SH,"
				+ "SI,SJ,SK,SL,SM,SN,SO,SR,ST,SV,SY,SZ,TC,TD,TF,TG,TH,TJ,TK,TM,TN,"
				+ "TO,TP,TR,TT,TV,TW,TZ,UA,UG,UM,US,UY,UZ,VA,VC,VE,VG,VI,VN,VU,WF," + "WS,YE,YT,YU,ZA,ZM,ZR,ZW")
						.indexOf(country);

		if (index % 3 != 0 || country.length() != 2)
			throw new MissingResourceException("Can't find ISO3 country for " + country, "java.util.Locale", country);

		// Don't read this aloud. These are the three letter country codes.
		return ("ANDAREAFGATGAIAALBARMANTAGOATAARGASMAUTAUSABWAZEBIHBRBBGDBELBFABGR"
				+ "BHRBDIBENBMUBRNBOLBRABHSBTNBVTBWABLRBLZCANCCKCAFCOGCHECIVCOKCHLCMR"
				+ "CHNCOLCRICUBCPVCXRCYPCZEDEUDJIDNKDMADOMDZAECUESTEGYESHERIESPETHFIN"
				+ "FJIFLKFSMFROFRAFXXGABGBRGRDGEOGUFGHAGIBGRLGMBGINGLPGNQGRCSGSGTMGUM"
				+ "GNBGUYHKGHMDHNDHRVHTIHUNIDNIRLISRINDIOTIRQIRNISLITAJAMJORJPNKENKGZ"
				+ "KHMKIRCOMKNAPRKKORKWTCYMKAZLAOLBNLCALIELKALBRLSOLTULUXLVALBYMARMCO"
				+ "MDAMDGMHLMKDMLIMMRMNGMACMNPMTQMRTMSRMLTMUSMDVMWIMEXMYSMOZNAMNCLNER"
				+ "NFKNGANICNLDNORNPLNRUNIUNZLOMNPANPERPYFPNGPHLPAKPOLSPMPCNPRIPRTPLW"
				+ "PRYQATREUROMRUSRWASAUSLBSYCSDNSWESGPSHNSVNSJMSVKSLESMRSENSOMSURSTP"
				+ "SLVSYRSWZTCATCDATFTGOTHATJKTKLTKMTUNTONTMPTURTTOTUVTWNTZAUKRUGAUMI"
				+ "USAURYUZBVATVCTVENVGBVIRVNMVUTWLFWSMYEMMYTYUGZAFZMBZARZWE").substring(index, index + 3);
	}

	/**
	 * Gets the country name suitable for display to the user, formatted for the
	 * default locale. This has the same effect as
	 * 
	 * <pre>
	 * getDisplayLanguage(Locale.getDefault());
	 * </pre>
	 *
	 * @return the language name of this locale localized to the default locale,
	 *         with the ISO code as backup
	 */
	public String getDisplayLanguage() {
		return getDisplayLanguage(defaultLocale);
	}

	/**
	 * <p>
	 * Gets the name of the language specified by this locale, in a form suitable
	 * for display to the user. If possible, the display name will be localized to
	 * the specified locale. For example, if the locale instance is
	 * <code>Locale.GERMANY</code>, and the specified locale is
	 * <code>Locale.UK</code>, the result would be 'German'. Using the German locale
	 * would instead give 'Deutsch'. If the display name can not be localized to the
	 * supplied locale, it will fall back on other output in the following order:
	 * </p>
	 * <ul>
	 * <li>the display name in the default locale</li>
	 * <li>the display name in English</li>
	 * <li>the ISO code</li>
	 * </ul>
	 * <p>
	 * If the language is unspecified by this locale, then the empty string is
	 * returned.
	 * </p>
	 *
	 * @param inLocale the locale to use for formatting the display string.
	 * @return the language name of this locale localized to the given locale, with
	 *         the default locale, English and the ISO code as backups.
	 * @throws NullPointerException if the supplied locale is null.
	 */
	public String getDisplayLanguage(Locale inLocale) {
		if (language.isEmpty())
			return "";

		if (inLocale.equals(Locale.ROOT)) // Base case
			return language;
		return getDisplayLanguage(getFallbackLocale(inLocale));
	}

	/**
	 * Returns the country name of this locale localized to the default locale. If
	 * the localized is not found, the ISO code is returned. This has the same
	 * effect as
	 * 
	 * <pre>
	 * getDisplayCountry(Locale.getDefault());
	 * </pre>
	 *
	 * @return the country name of this locale localized to the given locale, with
	 *         the ISO code as backup
	 */
	public String getDisplayCountry() {
		return getDisplayCountry(defaultLocale);
	}

	/**
	 * <p>
	 * Gets the name of the country specified by this locale, in a form suitable for
	 * display to the user. If possible, the display name will be localized to the
	 * specified locale. For example, if the locale instance is
	 * <code>Locale.GERMANY</code>, and the specified locale is
	 * <code>Locale.UK</code>, the result would be 'Germany'. Using the German
	 * locale would instead give 'Deutschland'. If the display name can not be
	 * localized to the supplied locale, it will fall back on other output in the
	 * following order:
	 * </p>
	 * <ul>
	 * <li>the display name in the default locale</li>
	 * <li>the display name in English</li>
	 * <li>the ISO code</li>
	 * </ul>
	 * <p>
	 * If the country is unspecified by this locale, then the empty string is
	 * returned.
	 * </p>
	 *
	 * @param inLocale the locale to use for formatting the display string.
	 * @return the country name of this locale localized to the given locale, with
	 *         the default locale, English and the ISO code as backups.
	 * @throws NullPointerException if the supplied locale is null.
	 */
	public String getDisplayCountry(Locale inLocale) {
		if (country.isEmpty())
			return "";

		if (inLocale.equals(Locale.ROOT)) // Base case
			return country;
		return getDisplayCountry(getFallbackLocale(inLocale));
	}

	/**
	 * Returns the variant name of this locale localized to the default locale. If
	 * the localized is not found, the variant code itself is returned. This has the
	 * same effect as
	 * 
	 * <pre>
	 * getDisplayVariant(Locale.getDefault());
	 * </pre>
	 *
	 * @return the variant code of this locale localized to the given locale, with
	 *         the ISO code as backup
	 */
	public String getDisplayVariant() {
		return getDisplayVariant(defaultLocale);
	}

	/**
	 * <p>
	 * Gets the name of the variant specified by this locale, in a form suitable for
	 * display to the user. If possible, the display name will be localized to the
	 * specified locale. For example, if the locale instance is a revised variant,
	 * and the specified locale is <code>Locale.UK</code>, the result would be
	 * 'REVISED'. Using the German locale would instead give 'Revidiert'. If the
	 * display name can not be localized to the supplied locale, it will fall back
	 * on other output in the following order:
	 * </p>
	 * <ul>
	 * <li>the display name in the default locale</li>
	 * <li>the display name in English</li>
	 * <li>the ISO code</li>
	 * </ul>
	 * <p>
	 * If the variant is unspecified by this locale, then the empty string is
	 * returned.
	 * </p>
	 *
	 * @param inLocale the locale to use for formatting the display string.
	 * @return the variant name of this locale localized to the given locale, with
	 *         the default locale, English and the ISO code as backups.
	 * @throws NullPointerException if the supplied locale is null.
	 */
	public String getDisplayVariant(Locale inLocale) {
		if (variant.isEmpty())
			return "";

		if (inLocale.equals(Locale.ROOT)) // Base case
			return country;
		return getDisplayVariant(getFallbackLocale(inLocale));
	}

	/**
	 * Gets all local components suitable for display to the user, formatted for the
	 * default locale. For the language component, getDisplayLanguage is called. For
	 * the country component, getDisplayCountry is called. For the variant set
	 * component, getDisplayVariant is called.
	 *
	 * <p>
	 * The returned String will be one of the following forms:<br>
	 * 
	 * <pre>
	 * language (country, variant)
	 * language (country)
	 * language (variant)
	 * country (variant)
	 * language
	 * country
	 * variant
	 * </pre>
	 *
	 * @return String version of this locale, suitable for display to the user
	 */
	public String getDisplayName() {
		return getDisplayName(defaultLocale);
	}

	/**
	 * Gets all local components suitable for display to the user, formatted for a
	 * specified locale. For the language component, getDisplayLanguage(Locale) is
	 * called. For the country component, getDisplayCountry(Locale) is called. For
	 * the variant set component, getDisplayVariant(Locale) is called.
	 *
	 * <p>
	 * The returned String will be one of the following forms:<br>
	 * 
	 * <pre>
	 * language (country, variant)
	 * language (country)
	 * language (variant)
	 * country (variant)
	 * language
	 * country
	 * variant
	 * </pre>
	 *
	 * @param locale locale to use for formatting
	 * @return String version of this locale, suitable for display to the user
	 */
	public String getDisplayName(Locale locale) {
		StringBuilder result = new StringBuilder();
		int count = 0;
		String[] delimiters = { "", " (", "," };
		if (language.length() != 0) {
			result.append(delimiters[count++]);
			result.append(getDisplayLanguage(locale));
		}
		if (country.length() != 0) {
			result.append(delimiters[count++]);
			result.append(getDisplayCountry(locale));
		}
		if (variant.length() != 0) {
			result.append(delimiters[count++]);
			result.append(getDisplayVariant(locale));
		}
		if (count > 1)
			result.append(")");
		return result.toString();
	}

	/**
	 * Does the same as <code>Object.clone()</code> but does not throw a
	 * <code>CloneNotSupportedException</code>. Why anyone would use this method is
	 * a secret to me, since this class is immutable.
	 *
	 * @return the clone
	 */
	@Override
	public Object clone() {
		// This class is final, so no need to use native super.clone().
		return new Locale(language, country, variant);
	}

	/**
	 * Return the hash code for this locale. The hashcode is the logical xor of the
	 * hash codes of the language, the country and the variant. The hash code is
	 * precomputed, since <code>Locale</code>s are often used in hash tables.
	 *
	 * @return the hashcode
	 */
	@Override
	public int hashCode() {
		return language.hashCode() ^ country.hashCode() ^ variant.hashCode();
	}

	/**
	 * Compares two locales. To be equal, obj must be a Locale with the same
	 * language, country, and variant code.
	 *
	 * @param obj the other locale
	 * @return true if obj is equal to this
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Locale))
			return false;
		Locale l = (Locale) obj;

		return (language == l.language && country == l.country && variant == l.variant);
	}

	/**
	 * <p>
	 * This method is used by the localized name lookup methods to retrieve the next
	 * locale to try. The next locale is derived from the supplied locale by
	 * applying the first applicable rule from the following:
	 * </p>
	 * <ol>
	 * <li>If the variant contains a <code>'_'</code>, then this and everything
	 * following it is trimmed.</li>
	 * <li>If the variant is non-empty, it is converted to an empty string.</li>
	 * <li>If the country is non-empty, it is converted to an empty string.</li>
	 * <li>If the language is non-empty, it is converted to an empty string (forming
	 * {@link java.util.Locale#ROOT})</li>
	 * </ol>
	 * <p>
	 * The base fallback locale is {@link java.util.Locale#ROOT}.
	 * </p>
	 *
	 * @param locale the locale for which a localized piece of data could not be
	 *               obtained.
	 * @return the next fallback locale to try.
	 */
	public static Locale getFallbackLocale(Locale locale) {
		String language = locale.getLanguage();
		String country = locale.getCountry();
		String variant = locale.getVariant();
		int uscore = variant.indexOf('_');
		if (uscore != -1)
			return new Locale(language, country, variant.substring(0, uscore));
		if (!variant.isEmpty())
			return new Locale(language, country, "");
		if (!country.isEmpty())
			return new Locale(language, "", "");
		return Locale.ROOT;
	}
} // class Locale
