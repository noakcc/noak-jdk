package jvm.charset;

import java.io.UnsupportedEncodingException;

import jvm.charset.ICharsetDecoder;
import jvm.charset.Latin1Decoder;
import jvm.charset.UTF8Decoder;

public class CharsetCoder {

	public static ICharsetDecoder getDecoder(String charset) throws UnsupportedEncodingException {
		charset = charset.toLowerCase();
		if (charset.equals("iso-8859-1") || charset.equals("latin1"))
			return new Latin1Decoder();
		if (charset.equals("utf-8") || charset.equals("utf8"))
			return new UTF8Decoder();

		throw new UnsupportedEncodingException("unsupported encoding " + charset);
	}

	public static ICharsetEncoder getEncoder(String charset) throws UnsupportedEncodingException {
		charset = charset.toLowerCase();
		if (charset.equals("iso-8859-1") || charset.equals("latin1"))
			return new Latin1Encoder();
		if (charset.equals("utf-8") || charset.equals("utf8"))
			return new UTF8Encoder();

		throw new UnsupportedEncodingException("unsupported encoding " + charset);
	}
}
