package jvm.charset;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

public class CharsetInputStreamReader extends Reader {
	private static final int MIN_BUFFERSIZE = 16;

	private final ICharsetDecoder _coder;
	private final InputStream _is;
	private final byte[] _buffer;
	private int _offset;
	private int _limit;
	// cache for storing a low surrogate
	private char _low;

	public CharsetInputStreamReader(InputStream is, ICharsetDecoder coder, int buffersize) {
		super(is);

		this._is = is;
		this._coder = coder;

		if (buffersize < MIN_BUFFERSIZE)
			buffersize = MIN_BUFFERSIZE;

		if (coder.getMaxCharLength() > buffersize)
			throw new IllegalArgumentException("Buffer to small for given charset.");

		this._buffer = new byte[buffersize];
	}

	public int fillBuffer() throws IOException {
		int req = _coder.estimateByteCount(_buffer, _offset, _limit);
		int len = _limit - _offset;
		if (len < req) {
			int rem = _buffer.length - _offset;
			if (rem < _coder.getMaxCharLength()) {
				System.arraycopy(_buffer, _offset, _buffer, 0, len);
				_offset = 0;
				_limit = len;
			}

			do {
				int tmp = _is.read(_buffer, _limit, _buffer.length - _limit);
				if (tmp < 0)
					// len is still smaller then req
					return len;

				len += tmp;
				_limit += tmp;
				// update req, since initial value might have been an
				// approximation
				req = _coder.estimateByteCount(_buffer, _offset, _limit);
			} while (len < req);
		}
		return req;
	}

	@Override
	public int read() throws IOException {
		if (_low > 0) {
			char tmp = this._low;
			this._low = 0;
			return tmp;
		}

		int needed = this.fillBuffer();
		if (needed <= 0)
			return -1;

		int cp = this._coder.decode(_buffer, _offset, _limit);
		this._offset += needed;

		if (cp < Character.MIN_SUPPLEMENTARY_CODE_POINT)
			return cp;

		cp -= Character.MIN_SUPPLEMENTARY_CODE_POINT;
		this._low = (char) (cp & 0x3FF | Character.MIN_LOW_SURROGATE);
		return (cp >> 10) | Character.MIN_HIGH_SURROGATE;
	}

	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
		if (len < 1)
			return 0;

		int origoff = off;
		// there should always be room for two chars, so substract 1
		int endoff = off + len - 1;

		int needed;
		if (this._low > 0) {
			cbuf[off++] = this._low;
			this._low = 0;

			// don't fill buffer to avoid blocking
			needed = this._coder.estimateByteCount(_buffer, _offset, _limit);
		} else {
			// fill buffer
			needed = this.fillBuffer();
		}

		// no data available
		if (_limit <= 0)
			return -1;

		if (needed <= 0)
			return -1;

		if (off < endoff) {
			while (_limit - _offset >= needed) {
				int cp = this._coder.decode(_buffer, _offset, _limit);

				if (cp < Character.MIN_SUPPLEMENTARY_CODE_POINT) {
					cbuf[off++] = (char) cp;
				} else {
					cbuf[off++] = (char) ((cp >> 10) + Character.MIN_HIGH_SURROGATE);
					cbuf[off++] = (char) ((cp & 0x3F) + Character.MIN_LOW_SURROGATE);
				}

				this._offset += needed;

				if (off >= endoff)
					break;

				needed = this._coder.estimateByteCount(_buffer, _offset, _limit);
			}
		}

		return off - origoff;
	}

	@Override
	public void close() throws IOException {
		this._low = 0;
		this._offset = this._limit = 0;
		this._is.close();
	}
}
