package jvm.charset;

public class Latin1Decoder implements ICharsetDecoder {

	@Override
	public int decode(byte[] buf, int offset, int limit) {
		return buf[offset] & 0xFF;
	}

	@Override
	public int estimateByteCount(byte[] buf, int offset, int limit) {
		return 1;
	}

	@Override
	public int getMaxCharLength() {
		return 1;
	}
}
