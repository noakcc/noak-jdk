package jvm.charset;

public class Latin1Encoder implements ICharsetEncoder {

	@Override
	public int encode(int codepoint, byte[] target, int offset) {
		if (codepoint < 0 || codepoint > 0xFF)
			codepoint = '?';

		target[offset] = (byte) codepoint;
		return offset + 1;
	}

	@Override
	public int estimateByteCount(int codepoint) {
		return 1;
	}

	@Override
	public int getMaxCharLength() {
		return 1;
	}
}
