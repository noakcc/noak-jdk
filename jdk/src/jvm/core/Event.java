package jvm.core;

/**
 * This class allows communication of event data between the firmware and the
 * low level classes. It can be used to detect I/O completion, Port values
 * changing, button presses etc. To use create a class having the required
 * device type and filter (this may identify a particular port, or I/O
 * operation). Then call the waitEvent function to wait for events from the
 * firmware. This call will block until either the firmware signals an event or
 * the timeout occurs. Upon completion the eventData field will contain
 * information about the event(s) that have been reported. Events themselves are
 * normally reset by calling the associated function to read/write the
 * associated device. If an event is not cleared, it will be reported again in
 * subsequent calls to eventWait.
 *
 *
 * <p>
 * <b>NOTE:</b> This is a low level system interface and should probably not be
 * used directly by user code.
 * </p>
 *
 * @author Andy
 */
public final class Event {
	///////////////////////////////////////////////////////////////////
	// Note the following fields are all shared with the firmware "JEvent"
	// and should not be modified without care!
	private volatile int state;
	private Event sync;
	private int type;
	private int id;
	private int message;
	private int wParam;
	private int lParam;
	private long time;
	private int counter;

	/** Event type for no events */
	// public final static int NONE = 0;
	/** Event type for JVM events */
	public final static int JVM = 1;
	/** Event type for Driver events */
	public final static int DRIVER_NTY = 0x10;
	public final static int DRIVER_API = 0x11;

	// Internal state flags
	private final static int WAITING = 1;
	private final static int SET = 2;

	public final static int TIMEOUT = 1 << 31;

	/**
	 * Value used to make a timeout be forever.
	 */
	public final static long WAIT_FOREVER = 0x7fffffffffffffffL;

	// Internal number counter
	private static int number = 0;

	/**
	 * Register this event with the system. Events must be registered before they
	 * are waited on.
	 * 
	 * @return >= 0 if the event has been registered < 0 if not.
	 */
	private native int registerEvent();

	/**
	 * Unregister this event. After calling this function the event should not be
	 * waited on.
	 * 
	 * @return >= 0 if the event was unregistered < 0 if not
	 */
	private native int unregisterEvent();

	/**
	 * Wait for an event to occur or for the specified timeout.
	 * 
	 * @param timeout the timeout in ms.
	 * @return >= 0 if the event was occured < 0 if timeout
	 */
	public synchronized int waitEvent(long timeout) throws InterruptedException {
		if (timeout <= 0L)
			return TIMEOUT;
		state = WAITING;
		sync = this;
		try {
			wait(timeout);
			if ((state & SET) == 0)
				return TIMEOUT;
		} finally {
			state = 0;
		}
		return 0;
	}

	/**
	 * Create a new event ready for use.
	 * 
	 * @param type The event type.
	 * @param id   The event id, a custom 32-bit.
	 * @return The new event object.
	 */
	public static synchronized Event allocate(int type, int id) {
		Event event = new Event();
		event.type = type;
		event.id = id;
		number = event.registerEvent();
		if (number < 0)
			return null;
		return event;
	}

	/**
	 * Gets the error code
	 * 
	 * @return error code
	 */
	public static int getError() {
		return (number < 0) ? number : 0;
	}

	/**
	 * Gets the event type, JVM for DRIVER(NTY or API)
	 * 
	 * @return The type
	 */
	public int getType() {
		return this.type;
	}

	/**
	 * Gets the event id, a custom 32-bit id
	 * 
	 * @return The id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Gets the event message source identifier
	 * 
	 * @return The message
	 */
	public int getMessage() {
		return this.message;
	}

	/**
	 * Gets the event 32-bit message specific additional information
	 * 
	 * @return The wParam
	 */
	public int getwParam() {
		return this.wParam;
	}

	/**
	 * Gets the event 32-bit message specific additional information
	 * 
	 * @return The lParam
	 */
	public int getlParam() {
		return this.lParam;
	}

	/**
	 * Gets the event counter of same occurred
	 * 
	 * @return The lParam
	 */
	public int getCounter() {
		return this.counter;
	}

	/**
	 * Gets the occur time of the event,custom time
	 * 
	 * @return The time
	 */
	public long getTime() {
		return this.time;
	}

	/**
	 * Release an event.
	 */
	public void free() {
		this.unregisterEvent();
	}
}
