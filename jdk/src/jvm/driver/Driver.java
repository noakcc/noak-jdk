package jvm.driver;

import java.io.IOException;

import jvm.core.Event;
import jvm.core.VM;

/**
 * 驱动类
 */
public final class Driver {

	private int _id = 0;

	private IDriverEventListener _eventListener = null;

	private Event _event = null;

	public final static long WAIT_FOREVER = 0x7fffffffffffffffL;

	/**
	 * 对象构造
	 * 
	 * @param id 驱动源标识, 负数表示内部驱动源, 0-32767表示外部驱动源
	 */
	public Driver(int id) {
		this._id = id;
	}

	/**
	 * 内部驱动调用
	 * 
	 * @param args 可变对象参数
	 * @return 返回值
	 */
	public int __invoke(Object... args) {
		return VM.driverInvoke(0, this._id, args);
	}

	/**
	 * 外部驱动同步调用
	 * 
	 * @param code 调用码, 0-65535
	 * @param args 可变对象参数
	 * @return 返回值
	 */
	public int invokeSync(int code, Object... args) {
		int newId = ((this._id & 0x7fff) << 16) | (code & 0xffff);
		return VM.driverInvoke(0, newId, args);
	}

	/**
	 * 外部驱动异步调用
	 * 
	 * @param lock 调用锁对象
	 * @param code 调用码, 0-65535
	 * @param args 可变对象参数
	 * @return 返回值
	 * @throws IOException
	 */
	public int invokeAsync(Object lock, int code, Object... args) throws IOException {
		synchronized (lock) {
			Event evt = Event.allocate(Event.DRIVER_API, this._id);
			if (evt == null) {
				throw new IOException("no space for invoke, code=" + Event.getError());
			}
			int newId = ((this._id & 0x7fff) << 16) | (code & 0xffff);
			if (VM.driverInvoke(1, newId, args) <= 0) {
				evt.free();
				throw new IOException("invoke fault.");
			}
			try {
				evt.waitEvent(Event.WAIT_FOREVER);
			} catch (InterruptedException e) {
			}
			int nRet = evt.getMessage();
			evt.free();
			return nRet;
		}
	}

	/**
	 * 注册事件监听
	 * 
	 * @param eventListener 监听对象
	 * @throws IOException
	 */
	public void registerEventListener(IDriverEventListener eventListener) throws IOException {
		synchronized (this) {
			if (this._event == null) {
				this._event = Event.allocate(Event.DRIVER_NTY, this._id);
				if (this._event == null) {
					throw new IOException("no space for event, code=" + Event.getError());
				}
			}
			if (this._eventListener != eventListener) {
				this._eventListener = eventListener;
			}
		}
	}

	/**
	 * 等待事件
	 * 
	 * @param timeout 等待超时，单位：毫秒
	 * @return 是否等到事件
	 * @throws InterruptedException
	 */
	public boolean waitEvent(long timeout) throws InterruptedException {
		if (this._event == null) {
			throw new NullPointerException("the event is null.");
		}
		int wait = this._event.waitEvent(timeout);
		if (wait >= 0 && this._eventListener != null) {
			int id = this._event.getId();
			int message = this._event.getMessage();
			int wParam = this._event.getwParam();
			int lParam = this._event.getlParam();
			int counter = this._event.getCounter();
			long time = this._event.getTime();
			this._eventListener.onEvent(id, message, wParam, lParam, counter, time);
		}
		return (wait >= 0) ? true : false;
	}

	/**
	 * 取消注册事件监听
	 */
	public void unregisterEventListener() {
		synchronized (this) {
			if (this._event != null) {
				this._event.free();
				this._event = null;
			}
			if (this._eventListener != null) {
				this._eventListener = null;
			}
		}
	}

	/**
	 * 获取事件监听是否已经注册
	 * 
	 * @return 是否
	 */
	public boolean isEventListenerRegistered() {
		return (this._event != null) ? true : false;
	}
}
