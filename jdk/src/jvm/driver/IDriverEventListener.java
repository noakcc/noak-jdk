package jvm.driver;

/**
 * 驱动事件接口
 */
public interface IDriverEventListener {

	/**
	 * 事件到达
	 * 
	 * @param source  事件标识
	 * @param message 事件消息，用户驱动自定义
	 * @param wParam  事件参数，用户驱动自定义
	 * @param lParam  事件参数，用户驱动自定义
	 * @param counter 事件计数器，发生的相同事件
	 * @param time    事件发生时间戳，用户驱动生成
	 */
	public void onEvent(int id, int message, int wParam, int lParam, int counter, long time);

}
