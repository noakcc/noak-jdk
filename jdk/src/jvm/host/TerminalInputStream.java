package jvm.host;

import java.io.IOException;
import java.io.InputStream;

import jvm.core.VM;

/**
 * A terminal input stream.
 * 
 * Used by System.in.*.
 * 
 * @author Andy
 *
 */
public class TerminalInputStream extends InputStream {
	@Override
	public int available() throws IOException {
		return VM.terminalAvailable();
	}

	@Override
	public int read() throws IOException {
		return VM.terminalRead();
	}
}
