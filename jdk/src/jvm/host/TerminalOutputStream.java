package jvm.host;

import java.io.IOException;
import java.io.OutputStream;

import jvm.core.VM;

/**
 * A terminal output stream
 * 
 * Used by System.out.*.
 * 
 * @author Andy
 *
 */
public class TerminalOutputStream extends OutputStream {
	@Override
	public void write(int b) throws IOException {
		VM.terminalWrite(b);
	}

	@Override
	public void flush() throws IOException {
		VM.terminalFlush();
	}
}
