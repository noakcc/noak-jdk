package noak.manager;

import java.io.IOException;

import jvm.core.VM;
import jvm.driver.Driver;

public class App {

	private static Driver driver;

	static {
		driver = new Driver(-3);
	}

	/**
	 * 获取应用存储区空间
	 * 
	 * @return 空间字节数
	 */
	public static int getTotalSpace() {
		return driver.__invoke(0);
	}

	/**
	 * 获取应用存储区剩余空间
	 * 
	 * @return 空间字节数
	 */
	public static int getFreeSpace() {
		return driver.__invoke(1);
	}

	/**
	 * 加载运行指定应用
	 * 
	 * @param name 应用名字
	 * @throws IOException
	 */
	public static void load(String name) throws IOException {
		int ret = driver.__invoke(2, name.getBytes());
		if (ret != 0) {
			throw new IOException("load(" + ret + ")");
		}
	}

	/**
	 * 设置上电自动加载的应用，掉电不丢失配置
	 * 
	 * @param name 应用名字
	 * @throws IOException
	 */
	public static void setAutoLoad(String name) throws IOException {
		int ret = driver.__invoke(3, name.getBytes());
		if (ret != 0) {
			throw new IOException("setAutoLoad(" + ret + ")");
		}
	}

	/**
	 * 获取当前正在加载运行应用的名字
	 * 
	 * @return 应用名字
	 */
	public static String getLoadName() {
		byte[] name = new byte[64];
		int size = driver.__invoke(4, name);
		if (size < 0) {
			return "";
		}
		return new String(name, 0, size);
	}

	/**
	 * 在应用存储区分配一块应用存储
	 * 
	 * @param name 应用名字
	 * @param size 应用大小
	 * @return 存储区域句柄
	 * @throws IOException
	 */
	public static int alloc(String name, int size) throws IOException {
		int handle = driver.__invoke(5, name.getBytes(), size);
		if (handle <= 0) {
			throw new IOException("alloc(" + handle + ")");
		}
		return handle;
	}

	/**
	 * 写指定应用存储区
	 * 
	 * @param handle 存储区句柄
	 * @param data   应用数据
	 * @throws IOException
	 */
	public static void write(int handle, byte[] data) throws IOException {
		int ret = driver.__invoke(6, handle, data);
		if (ret != 0) {
			throw new IOException("write(" + ret + ")");
		}
	}

	/**
	 * 安装指定应用存储区应用
	 * 
	 * @param handle 存储区句柄
	 * @throws IOException
	 */
	public static void install(int handle) throws IOException {
		int ret = driver.__invoke(7, handle);
		if (ret != 0) {
			throw new IOException("install(" + ret + ")");
		}
	}

	/**
	 * 应用区域枚举
	 * 
	 * @return 应用列表
	 */
	public static String[] enumerate() {
		synchronized (driver) {
			int n = driver.__invoke(9, null);
			byte[] buffer = new byte[n];
			driver.__invoke(9, buffer);
			String list = new String(buffer);
			return list.split(VM.getLineSeparator());
		}
	}
	
	/**
	 * 获取自动运行应用的名字
	 * 
	 * @return 应用名字
	 */
	public static String getAutoLoadName() {
		byte[] name = new byte[64];
		int size = driver.__invoke(10, name);
		if (size <= 0) {
			return "";
		}
		return new String(name, 0, size);
	}
}
