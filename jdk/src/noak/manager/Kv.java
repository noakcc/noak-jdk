package noak.manager;

import java.io.IOException;

import jvm.driver.Driver;

public class Kv {

	private static Driver driver;

	static {
		driver = new Driver(-1);
	}

	/**
	 * 获取指定键值数据
	 * 
	 * @param key 键值
	 * @return 键值数据
	 * @throws IOException
	 */
	synchronized public static byte[] get(String key) throws IOException {
		byte[] _key = key.getBytes();
		int sz = driver.__invoke(0, _key);
		if(sz <= 0) {
			throw new IOException("get(" + sz + ")");
		}
		byte[] value = new byte[sz];
		driver.__invoke(1, _key, value);
		return value;
	}

	/**
	 * 设置指定键值数据，没有则自动创建
	 * 
	 * @param key   键值
	 * @param value 键值数据
	 * @throws IOException
	 */
	public static void set(String key, byte[] value) throws IOException {
		int ret = driver.__invoke(2, key.getBytes(), value);
		if (ret != 0) {
			throw new IOException("set(" + ret + ")");
		}
	}

	/**
	 * 删除指定键值数据
	 * 
	 * @param key 键值
	 * @throws IOException
	 */
	public static void delete(String key) throws IOException {
		int ret = driver.__invoke(3, key.getBytes());
		if (ret != 0) {
			throw new IOException("delete(" + ret + ")");
		}
	}

	/**
	 * 格式化键值存储区
	 * 
	 * @throws IOException
	 */
	public static void format() throws IOException {
		int ret = driver.__invoke(4);
		if (ret != 0) {
			throw new IOException("format(" + ret + ")");
		}
	}
}
