package noak.manager;

import jvm.driver.Driver;

public class Log {

	/**
	 * 不打印任何日志
	 */
	public final static int NONE = 0;
	/**
	 * 仅打印错误日志
	 */
	public final static int ERROR = 1;
	/**
	 * 仅打印警告，信息，调试，冗余日志
	 */
	public final static int WRANING = 2;
	/**
	 * 仅打印信息，调试，冗余日志
	 */
	public final static int INFO = 3;
	/**
	 * 仅打印调试，冗余日志
	 */
	public final static int DEBUG = 4;
	/**
	 * 仅打印冗余日志
	 */
	public final static int VERBOSE = 5;
	/**
	 * 打印所有日志，默认
	 */
	public final static int ALL = 6;

	private static Driver driver;

	static {
		driver = new Driver(-2);
	}

	/**
	 * 获取当前日志打印等级
	 * 
	 * @return 等级值
	 */
	public static int getLevel() {
		return driver.__invoke(0);
	}

	/**
	 * 获取当前日志打印等级字符串
	 * 
	 * @return 等级字符串，如：ALL，INFO等
	 */
	public static String getLevelName() {
		int level = driver.__invoke(0);
		switch (level) {
			case 0:
				return "NONE";
			case 1:
				return "ERROR";
			case 2:
				return "WRANING";
			case 3:
				return "INFO";
			case 4:
				return "DEBUG";
			case 5:
				return "VERBOSE";
			case 6:
				return "ALL";
			default:
				return "";
		}
	}

	/**
	 * 设置日志打印等级
	 * 
	 * @param level 等级值
	 */
	public static void setLevel(int level) {
		driver.__invoke(1, level);
	}

	/**
	 * 打印日志
	 * 
	 * @param level 词此条日志等级
	 * @param text  日志内容，可支持中文
	 */
	public static void print(int level, String text) {
		byte[] bytes = text.getBytes();
		byte[] buffer = new byte[bytes.length + 1];
		System.arraycopy(bytes, 0, buffer, 0, bytes.length);
		driver.__invoke(2, level, buffer, buffer.length - 1);
	}
}
