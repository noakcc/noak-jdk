package noak.manager;

import jvm.driver.Driver;

public class Shell {

	private static Driver driver;

	static {
		driver = new Driver(-4);
	}

	/**
	 * 获取'壳'参数串
	 * 
	 * @return 参数串
	 */
	synchronized public static String getArgs() {
		int sz = driver.__invoke(0);
		if (sz <= 0) {
			return "";
		}
		byte[] buffer = new byte[sz];
		sz = driver.__invoke(1, buffer);
		if (sz < 0) {
			throw new ArrayIndexOutOfBoundsException("" + sz);
		}
		return new String(buffer);
	}

	/**
	 * 设置'壳'参数串
	 * 
	 * @param args 参数串
	 */
	public static void setArgs(String args) {
		int sz = driver.__invoke(2, args.getBytes());
		if (sz < 0) {
			throw new VirtualMachineError();
		}
	}

	/**
	 * 获取'壳'版本串，即：框架版本
	 * 
	 * @return 版本串
	 */
	public static String getVersion() {
		byte[] buffer = new byte[32];
		int sz = driver.__invoke(3, buffer);
		if (sz <= 0) {
			throw new ArrayIndexOutOfBoundsException("" + sz);
		}
		return new String(buffer, 0, sz);
	}
}
