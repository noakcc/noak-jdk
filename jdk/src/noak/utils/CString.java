package noak.utils;

import java.io.UnsupportedEncodingException;

import jvm.charset.CharsetCoder;
import jvm.charset.ICharsetEncoder;
import jvm.charset.UTF8Encoder;

public class CString {

    /**
     * 转换为C语言格式的UTF-8字符串数组
     * 
     * @param str Java字符串对象
     * @return C字符串数组, 以'\'0结束
     */
    public static byte[] getCBytes(String str) {
        int bNum = 0, bOffset = 0;
        ICharsetEncoder encoder = new UTF8Encoder();
        char[] val = str.toCharArray();
        for (int i = 0; i < val.length; i++)
            bNum += encoder.estimateByteCount(val[i]);
        byte[] b = new byte[bNum + 1]; /* C字符缓冲区包含'\0' */
        for (int i = 0; i < val.length; i++)
            bOffset = encoder.encode(val[i], b, bOffset);
        return b;
    }

    /**
     * 转换为C语言格式的指定编码的字符串数组
     * 
     * @param str     Java字符串对象
     * @param charset 编码格式
     * @return C字符串数组, 以'\'0结束
     * @throws UnsupportedEncodingException
     */
    public static byte[] getCBytes(String str, String charset) throws UnsupportedEncodingException {
        int bNum = 0, bOffset = 0;
        ICharsetEncoder encoder = CharsetCoder.getEncoder(charset);
        char[] val = str.toCharArray();
        for (int i = 0; i < val.length; i++)
            bNum += encoder.estimateByteCount(val[i]);
        byte[] b = new byte[bNum + 1]; /* C字符缓冲区包含'\0' */
        for (int i = 0; i < val.length; i++)
            bOffset = encoder.encode(val[i], b, bOffset);
        return b;
    }
}
