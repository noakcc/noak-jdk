package noak.utils;

import jvm.driver.Driver;

public class Ctime {

    private static Driver driver;

    static {
        driver = new Driver(-5);
    }

    /**
     * C语言标准函数调用，把tm所指向的结构转换为自1970年1月1日以来持续时间的秒数
     * 
     * @param tm 日期数据结构
     *           int[0] tm_sec, 秒，范围从0到59
     *           int[1] tm_min,分，范围从0到59
     *           int[2] tm_hour,小时，范围从0到23
     *           int[3] tm_mday,一月中的第几天，范围从1到31
     *           int[4] tm_mon,月份，范围从0到11
     *           int[5] tm_year,自1900起的年数
     *           int[6] tm_wday,一周中的第几天，范围从0到6
     *           int[7] tm_yday,一年中的第几天，范围从0到365
     *           int[8] tm_isdst,夏令时
     * @return 时间戳秒数, <0:有错误
     */
    public static long mktime(int[] tm) {
        long[] timestamp = new long[] { -1 };
        driver.__invoke(0, tm, timestamp);
        return timestamp[0];
    }

    /**
     * C语言标准函数调用，把自1970年1月1日以来持续时间的秒数转换为tm结构
     * 
     * @param timestamp 时间戳秒数
     * @return tm结构, 格式同上'mktime'
     */
    public static int[] gmtime(long timestamp) {
        int[] tm = new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1 };
        driver.__invoke(1, timestamp, tm);
        return tm;
    }

}
