package noak.utils;

import jvm.core.VM;

/**
 * Simple collection of time delay routines that are non interruptable.
 * 
 * @author NOAK
 */
public class Delay {

    private Delay() {
    }

    /**
     * Wait for the specified number of milliseconds. Delays the current thread for
     * the specified period of time. Can not be interrupted (but it does preserve
     * the interrupted state).
     * 
     * @param period time to wait in ms
     */
    public static void msDelay(long period) {
        if (period <= 0)
            return;
        long end = VM.tickMillSeconds() + period;
        boolean interrupted = false;
        do {
            try {
                Thread.sleep(period);
            } catch (InterruptedException ie) {
                interrupted = true;
            }
            period = end - VM.tickMillSeconds();
        } while (period > 0);

        if (interrupted)
            Thread.currentThread().interrupt();
    }

    /**
     * Wait for the specified number of seconds. Delays the current thread for the
     * specified period of time. Can not be interrupted.
     * 
     * @param period time to wait in us
     */
    public static void sDelay(long period) {
        msDelay(period * 1000);
    }
}
